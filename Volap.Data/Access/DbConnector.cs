﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using NLog;

namespace Volap.Data.Access
{
    public class DbConnector
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private static int _timeout = Convert.ToInt32(ConfigurationManager.AppSettings["timeout"]);

        public static Dictionary<int, string> CubeConnDict = new Dictionary<int, string>();


        public static DataTable GenerateDataTable(int cubeId, string query, params SqlParameter[] parameters)
        {
            string connString;
            CubeConnDict.TryGetValue(cubeId, out connString);
            if (String.IsNullOrEmpty(connString)) return null;

            var conn = new SqlConnection(connString);

            DataTable dataTable = GenerateDataTable(query, conn, parameters);
            try
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            catch (SqlException sqle)
            {
                _logger.Error(sqle.Message);
            }
            return dataTable;
        }

        public static DataTable GenerateDataTable(string query, SqlConnection conn, params SqlParameter[] parameters)
        {
            DataTable dataTable = null;
            try
            {
                if (conn != null && conn.State == ConnectionState.Closed || conn.State == ConnectionState.Broken)
                    conn.Open();

                var cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = _timeout;
                if (parameters != null && parameters.Length > 0)
                {
                    cmd.Parameters.AddRange(parameters);
                }
                var adapter = new SqlDataAdapter { SelectCommand = cmd };
                dataTable = new DataTable();
                adapter.Fill(dataTable);
                adapter.Dispose();
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            catch (Exception sqle)
            {
                _logger.Error(sqle.Message);
                throw sqle;
            }
            return dataTable;
        }

        public static int GenerateScalar_Int(string query, SqlConnection conn, params SqlParameter[] parameters)
        {
            int val = 0;
            var cmd = new SqlCommand(query, conn);
            cmd.CommandTimeout = _timeout;

            try
            {
                if (conn != null && (conn.State == ConnectionState.Closed || conn.State == ConnectionState.Broken))
                    conn.Open();
                if (parameters != null && parameters.Length > 0)
                {
                    cmd.Parameters.AddRange(parameters);
                }
                object o = cmd.ExecuteScalar();
                val = DBNull.Value != o ? Convert.ToInt32(o) : 0;
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            catch (SqlException sqle)
            {
                //_logger.Error(sqle.Message);
                throw (sqle);
            }
            cmd.Dispose();
            return val;
        }

        /// <summary>
        /// This method returns a single integer value.
        /// </summary>
        /// <param name="query">a sql query statement</param>
        /// <param name="parameters">sql parameters collection</param>
        /// <returns></returns>
        public static int GenerateScalar_Int(int cubeId, string query, params SqlParameter[] parameters)
        {
            string connString;
            CubeConnDict.TryGetValue(cubeId, out connString);
            if (String.IsNullOrEmpty(connString)) return -1;

            using (var conn = new SqlConnection(connString))
            {
                return GenerateScalar_Int(query, conn, parameters);
            }
        }

    }
}
