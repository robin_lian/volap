﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Elasticsearch.Net;
using Nest;
using NLog;
using ServiceStack;
using ServiceStack.Text;
using Volap.Data.Models;
using Volap.Data.Search;

namespace Volap.Data.Access
{
    public class SearchInvoker
    {

        private readonly ElasticClient _client = new ElasticClient(VolapConfig.Settings());
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly Stopwatch _stopWatch = new Stopwatch();

        private readonly SearchContract _contract;
        private const int DocCount = 20;
        private readonly string _dateColumn;

        public SearchInvoker(SearchContract contract)
        {
            _contract = contract;

            using (FluentModel dbContext = new FluentModel())
            {
                IQueryable<MeasureGroup> mGroups = from mg in dbContext.MeasureGroups where mg.Name.ToLower().Equals(contract.MeasureGroup)
                                                   select mg;
                var firstOrDefault = mGroups.FirstOrDefault();
                if (firstOrDefault != null) 
                    _dateColumn = firstOrDefault.DateColumn.Trim();
            }
        }

        public IList<Doc> GetDocs()
        {
            var searchDesc = new SearchDescriptor<object>();
            searchDesc.Index(_contract.Index).Type(_contract.MeasureGroup);

            // todo MultiMatch
            if (_contract.FromDate != null || _contract.ToDate != null || _contract.GetQueryFacets().Count > 0) // has date(s) or filter(s)
            {
                if (string.IsNullOrEmpty(_contract.QueryString))
                    searchDesc.Query(q => q.Bool(b => b.Must(GenerateFilters(_contract)))); // no need Query(fq => fq.MatchAll())
                else
                    searchDesc.Query(q => q.Bool(b => b.Filter(GenerateFilters(_contract))));
                //.Must(n => n.MultiMatch(m => m.Query(_contract.QueryString).Fields(_contract.AllFields)))));
                //Query(fq => fq.MultiMatch(m => m.Fields(f=>f. ).Query(_contract.QueryString)))));
            }
            //else searchDesc.Query(fq => fq.MultiMatch(m => m.Fields( _contract.AllFields).Query(_contract.QueryString)));


            //if (_contract.HasQueryDates()) SetQueryDates(_contract, searchContract);

            // sort action - column and sort type from grid
            if (!string.IsNullOrEmpty(_contract.Sort))
                searchDesc.Sort(s => s.Field(f=>f.Ascending()));  // _contract.Sort .Ascending()

            // paging
            searchDesc.Take(DocCount);

            var response = _client.Search<dynamic>(searchDesc);
            //ElasticsearchResponse<string> response = _client.Raw.Search<string>(_client.Serializer.Serialize(searchDesc));

            List<Doc> docs = null;

            if (response.IsValid)
            {
                // get docids from hits json
                var hresults = JsonObject.Parse(response.ToJson());

                List<Hit> hits = hresults.Object("hits").ArrayObjects("hits").ConvertAll(x => new Hit
                {
                    Id = x.JsonTo<int>("_id")
                    //Score = x.JsonTo<float>("_score")
                }); // to store in cache  DynamicDictionary

                // get multi-get docs using only core fields
                var docResponse = _client.MultiGet(mg => mg.Index(_contract.Index).Type(_contract.MeasureGroup));
                    // .GetMany(new { ids = hits.Select(h => h.Id).ToArray() } , o=>o.Fields(f=>f.Field("field"), ) ));
                //ElasticsearchResponse<string> docResponse = _client.Raw.Mget<string>(_contract.Index, _contract.MeasureGroup, 
                //    new { ids = hits.Select(h => h.Id).ToArray() },q => q.Fields(_contract.GetCoreFields()));

                if (docResponse.IsValid)
                {
                    var hitsArr = JsonObject.Parse(docResponse.ToJson());

                    docs = hitsArr.ArrayObjects("docs").ConvertAll(x => new Doc
                    {
                        Id = x.JsonTo<int>("_id"),
                        Fields = x.Get("fields")
                    });
                }

            }
            return docs;
        }

        private Func<QueryContainerDescriptor<object>, QueryContainer> GenerateFilters(SearchContract searchContract)
        {
            int count = searchContract.GetQueryFacets().Count;

            switch (count)
            {
                case 1:
                    string[] fa = searchContract.GetQueryFacets()[0].Split(':');
                    fa[1] = fa[1].ReplaceAll("\"", "");
                    if (_contract.FromDate == null && _contract.ToDate == null)
                        return f => f.Term(fa[0], fa[1]);

                    if (_contract.FromDate == null && _contract.ToDate != null)
                        return f => f.Term(fa[0], fa[1]) && f.Range(r => r.Field(searchContract.DateField).
                        LessThanOrEquals(Convert.ToDouble(searchContract.ToDate)));

                    if (_contract.FromDate != null && _contract.ToDate == null)
                        return f => f.Term(fa[0], fa[1]) && f.Range(r => r.Field(searchContract.DateField).
                            GreaterThanOrEquals(Convert.ToDouble(searchContract.FromDate)));

                    if (_contract.FromDate != null && _contract.ToDate != null)
                        return f => f.Term(fa[0], fa[1]) && f.Range(r => r.Field(searchContract.DateField).LessThanOrEquals(Convert.ToDouble(searchContract.ToDate)))
                               && f.Range(r => r.Field(searchContract.DateField).GreaterThanOrEquals(Convert.ToDouble(searchContract.FromDate)));
                    break;

                /*case 2:
                    string[] f1 = searchContract.GetQueryFacets()[0].Split(':');
                    string[] f2 = searchContract.GetQueryFacets()[1].Split(':');
                    return fq => fq.Filter(f => f.Bool(b => b.Must(t => t.Term(f1[0], f1[1]) && t.Term(f2[0], f2[1]))));
                 
                 
                 */

                    
            }
            return null;
        }


        // add filter range for dateColumn 
        /*private void SetQueryDates(SearchContract sd, SearchDescriptor<object> searchDesc)
        {
            if (sd.FromDate == null)
                return;

            if (sd.ToDate == null)
                searchDesc.Filter(ff => ff.Range(r => r.OnField(_dateColumn).GreaterOrEquals(sd.FromDate)));

            else
                searchDesc.Filter(ff => ff.Range(r => r.OnField(_dateColumn).GreaterOrEquals(sd.FromDate)) &&
                          ff.Range(r => r.OnField(_dateColumn).LowerOrEquals(sd.ToDate)));

        }*/
    }
}
