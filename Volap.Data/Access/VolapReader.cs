﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Elasticsearch.Net;
using Nest;
using NLog;
using ServiceStack.Text;
using Volap.Data.Search;
using Volap.Data.Utils;

namespace Volap.Data.Access
{
    public class VolapReader
    {
        private readonly ElasticClient _client = new ElasticClient(VolapConfig.Settings());
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly Stopwatch _stopWatch = new Stopwatch();

        private readonly OlapContract _contract;

        public VolapReader()
        {
            _contract = new OlapContract();
            ServiceStackHelper.Help();

            TestDate();
        }

        public VolapReader(OlapContract contract)
        {
            _contract = contract;
            ServiceStackHelper.Help();
        }


        private void TestDate()
        {
            // write lastUpdate
            //var searchDesc = new SearchDescriptor<dynamic>();
            //searchDesc.Query(q => q.MatchAll()).Size(1).Sort(s => s.Descending(new Field {Name = "lastupdate"}));  //.SortDescending("lastupdate");

            var responseDate = _client.Search<dynamic>(se=>se.Query(q => q.MatchAll()).Size(1).Sort(s => s.Descending(new Field { Name = "lastupdate" })));

            if (responseDate.IsValid)
            {
                //var results = JsonObject.Parse(responseDate.Response);
                var hits = responseDate.Hits;

                /*string date = results.Object("hits").ArrayObjects("hits").GetDate("incident");

                if (string.IsNullOrEmpty(date))
                    date = DateHelper.ToUnixTime(DateTime.UtcNow).ToString();*/

            }

        }

        /*public IList<Facet> GetFacets()
        {
            _stopWatch.Start();
            _logger.Info("Running Olap : {0} by {1} @ " + DateTime.Now, _contract.Measure, _contract.GroupBy);

            var searchDesc = new SearchDescriptor<object>();
            searchDesc.Index(_contract.Index).Type(_contract.MeasureGroup).Size(0);

            if (!string.IsNullOrEmpty(_contract.Slicer1) || !string.IsNullOrEmpty(_contract.Filter))
                searchDesc.Aggregations(a1 => a1.Filter("filtered", f => f.Filter(fl => fl.Bool(b => b.Must(GenerateFilters(_contract).ToArray()))).
                    Aggregations(GenerateGroupBy(_contract))));
            else
                searchDesc.Aggregations(GenerateGroupBy(_contract));

            ElasticsearchResponse<string> response = _client.Raw.Search<string>(_client.Serializer.Serialize(searchDesc));
            IList<Facet> facets = null;

            if (response.Success && response.Response != null)
            {
                var results = JsonObject.Parse(response.Response);

                facets = results.Object("aggregations").Object("filtered").Object("groupby").ArrayObjects("buckets").ConvertAll(x => new Facet
                {
                    Key = x.JsonTo<string>("key_as_string"),
                    //Count = x.JsonTo<int>("doc_count"),g
                    Value = x.Object("measure").JsonTo<float>("value")
                });
            }

            _logger.Info("End Olap Search");
            _stopWatch.Stop();
            _logger.Info(string.Format("Time taken {0} to search N results", _stopWatch.Elapsed.TotalSeconds)); // count

            return facets;
        }*/

        /*private void RunOlapDates_R()
        {
            long count = 0;
            const string index = "arsystem";
            const string measureGroup = "helpdesk";
            // type multi_field
            ElasticsearchResponse<string> response;

            _stopWatch.Start();
            _logger.Info("Running Olap Dates : businesstime by time @ " + DateTime.Now);

            var contract = new OlapContract();
            
            var searchDesc = new SearchDescriptor<object>();
            searchDesc.Index(index).Type(measureGroup).Size(0);

            if (!string.IsNullOrEmpty(contract.Slicer1) && !string.IsNullOrEmpty(contract.Filter))
                searchDesc.Aggregations(a1 => a1.Filter("filtered", f => f.Filter(fl => fl.Bool(b => b.Must(GenerateFilters(contract).ToArray()))).
                    Aggregations(GenerateGroupBy(contract))));
            else
                searchDesc.Aggregations(GenerateGroupBy(contract));

            response = _client.Raw.Search<string>(_client.Serializer.Serialize(searchDesc));

            if (response.Success && response.Response != null)
            {
                var results = JsonObject.Parse(response.Response);

                IList<Facet> facets = results.Object("aggregations").Object("filtered").Object("groupby").ArrayObjects("buckets").ConvertAll(x => new Facet
                {
                    Key = x.JsonTo<string>("key_as_string"),
                    Count = x.JsonTo<int>("doc_count"),
                    Value = x.Object("measure").JsonTo<float>("value")
                    //Score = x.JsonTo<float>("_score")
                });
            }
        }*/

        #region filter
        // Generate bool filters
        private IEnumerable<Func<QueryContainerDescriptor<object>, QueryContainer>> GenerateFilters(OlapContract contract)
        {
            // add MUST for every contract
            if (!string.IsNullOrEmpty(contract.Slicer1))
            {
                Func<QueryContainerDescriptor<object>, QueryContainer> fc;

                if (!string.IsNullOrEmpty(contract.Slicer2))  // add slicer2
                {
                    string[] s1 = contract.Slicer1.Split(':');
                    string[] s2 = contract.Slicer2.Split(':');

                    if (!string.IsNullOrEmpty(contract.Filter))
                    {
                        fc = q => q.Terms(t=>t.Name(s1[0]).Terms(s1[1].Split(',').ToList())) && q.Terms(t => t.Name(s2[0]).Terms(s2[1].Split(',').ToList())) &&
                            //q.Terms(s2[0], s2[1].Split(',').ToList()) 
                            q.Range(GetRangeFilterDescriptor(contract.Filter));
                    }
                    else
                        fc = q => q.Terms(t => t.Name(s1[0]).Terms(s1[1].Split(',').ToList())) && q.Terms(t => t.Name(s2[0]).Terms(s2[1].Split(',').ToList()));

                    yield return fc;
                }
                else
                {
                    string[] s1 = contract.Slicer1.Split(':'); // only add slicer1

                    if (!string.IsNullOrEmpty(contract.Filter))
                    {
                        fc = q => q.Terms(t => t.Name(s1[0]).Terms(s1[1].Split(',').ToList())) && q.Range(GetRangeFilterDescriptor(contract.Filter)); // add measure range filter
                    }
                    else
                    {

                        if (s1[1].Contains(","))
                            fc = q => q.Terms(t => t.Name(s1[0]).Terms(s1[1].Split(',').ToList())); // match multi terms on single slicer1
                        else
                            fc = q => q.Term(s1[0], s1[1]);

                    }
                    yield return fc;
                }
            }
        }

        // add measure filter
        private static Func<NumericRangeQueryDescriptor<object>, INumericRangeQuery> GetRangeFilterDescriptor(string filter)
        {
            if (filter.Contains("<"))
            {
                string[] fa = filter.Split('<');
                if (fa[1].StartsWith("="))
                    return rf => rf.Field(fa[0]).LessThanOrEquals(Convert.ToDouble(fa[1].Replace("=", "")));

                return rf => rf.Field(fa[0]).LessThan(Convert.ToDouble(fa[1]));
            }
            if (filter.Contains(">"))
            {
                string[] fa = filter.Split('>');
                if (fa[1].StartsWith("="))
                    return rf => rf.Field(fa[0]).GreaterThanOrEquals(Convert.ToDouble(fa[1].Replace("=", "")));

                return rf => rf.Field(fa[0]).GreaterThan(Convert.ToDouble(fa[1]));
            }
            return null;
        }
        #endregion



        #region Olap
        private Func<AggregationContainerDescriptor<object>, IAggregationContainer> GenerateGroupBy(OlapContract contract)
        {
            if (contract.GroupBy.IndexOf("date", StringComparison.OrdinalIgnoreCase) >= 0) // tpt? assume doc row count, eg incident
                return GenerateGroupByDates(contract);

            return GenerateGroupByDim(contract);
        }

        private static Func<AggregationContainerDescriptor<object>, IAggregationContainer> GenerateGroupByDates(OlapContract contract)
        {
            Func<AggregationContainerDescriptor<object>, IAggregationContainer> ad;
            string[] da = contract.GroupBy.Split(':');
            if (da.Length != 2) return null;

            if (!string.IsNullOrEmpty(contract.GroupBy))
            {
                if (contract.MeasureType.IndexOf("count", StringComparison.OrdinalIgnoreCase) >= 0) // tpt? assume doc row count, eg incident
                {
                    ad = a1 => a1.DateHistogram("groupby", t => t.Field(da[0]).Interval(da[1]));

                    return ad;
                }
                if (contract.MeasureType.IndexOf("sum", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    ad = a1 => a1.DateHistogram("groupby", t => t.Field(da[0]).Interval(da[1]).
                            Aggregations(a2 => a2.Sum("measure", v => v.Field(contract.Measure))));

                    return ad;
                }
                if (contract.MeasureType.IndexOf("average", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    ad = a1 => a1.DateHistogram("groupby", t => t.Field(da[0]).Interval(da[1]).
                            Aggregations(a2 => a2.Average("measure", v => v.Field(contract.Measure))));

                    return ad;
                }

                ad = a1 => a1.DateHistogram("groupby", t => t.Field(da[0]).Interval(da[1]).
                            Aggregations(a2 => a2.Stats("measure", v => v.Field(contract.Measure))));

                return ad;

            }
            return null;
        }


        // append groupby, measure then series
        private static Func<AggregationContainerDescriptor<object>, IAggregationContainer> GenerateGroupByDim(OlapContract contract)
        {
            Func<AggregationContainerDescriptor<object>, IAggregationContainer> ad;

            if (!string.IsNullOrEmpty(contract.GroupBy))
            {
                if (contract.MeasureType.IndexOf("count", StringComparison.OrdinalIgnoreCase) >= 0) // tpt? assume doc row count, eg incident
                {
                    ad = a1 => a1.Terms("groupby", t => t.Field(contract.GroupBy).
                        Aggregations(a2 => a2.ValueCount("measure", v => v.Field(contract.Measure))));

                    return ad;
                }
                if (contract.MeasureType.IndexOf("sum", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    ad = a1 => a1.Terms("groupby", t => t.Field(contract.GroupBy). // groupby
                         Aggregations(a2 => a2.Sum("measure", v => v.Field(contract.Measure))));

                    return ad;
                }
                if (contract.MeasureType.IndexOf("average", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    ad = a1 => a1.Terms("groupby", t => t.Field(contract.GroupBy). // groupby
                         Aggregations(a2 => a2.Average("measure", v => v.Field(contract.Measure))));

                    return ad;
                }

                ad = a1 => a1.Terms("groupby", t => t.Field(contract.GroupBy). // groupby
                        Aggregations(a2 => a2.Stats("measure", v => v.Field(contract.Measure))));

                return ad;
            }
            return null;
        }
        #endregion

    }
}
