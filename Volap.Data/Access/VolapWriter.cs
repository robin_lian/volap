﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using Elasticsearch.Net;
using Nest;
using NLog;
using ServiceStack.Text;
using Volap.Data.Models;
using Volap.Data.Search;
using Volap.Data.Utils;
using Field = Volap.Data.Models.Field;

namespace Volap.Data.Access
{
    public class VolapWriter
    {

        private ElasticClient _client = new ElasticClient(VolapConfig.Settings());
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly Stopwatch _stopWatch = new Stopwatch();
        private readonly string _destination;
        //private readonly string _fieldDataCache;

        private List<dynamic> _dynList = new List<dynamic>();
        private SortedList<string, dynamic> _sortedList = new SortedList<string, dynamic>();
        private List<int> _dateColumnList;
        private List<Type> _columnTypeList;
        //private List<string> _dateKeyList;

        // global field mappings
        private StringProperty _dimField;
        //private StringProperty _dimDvField;
        private StringProperty _analyzedField;
        private DateProperty _dateField;
        private NumberProperty _intField;
        private NumberProperty _intMeasureField;
        private NumberProperty _doubleMeasureField;
        private BooleanProperty _booleanField;


        public VolapWriter()
        {
            ServiceStackHelper.Help();
            //_fieldDataCache = ConfigurationManager.AppSettings["fielddata.cache"];
            _destination = ConfigurationManager.AppSettings["destination"];
            InitFields();

            const string sqlQuery = @"Select * from Cube where Active=1";

            using (FluentModel dbContext = new FluentModel())
            {
                IList<Cube> cubes = dbContext.ExecuteQuery<Cube>(sqlQuery);

                foreach (Cube cube in cubes)
                {
                    Console.WriteLine("Processing CubeId: {0}\n", cube.Id);

                    IList<Connection> conns = dbContext.ExecuteQuery<Connection>(string.Format("Select * from Connection where Id = {0}", cube.ConnectionId));
                    var firstOrDefault = conns.FirstOrDefault();
                    if (firstOrDefault != null)
                        DbConnector.CubeConnDict.Add(cube.Id, firstOrDefault.ConnectionString);

                    IndexData(cube.Id, cube.Name, dbContext);
                }
            }
        }

        private void InitFields()
        {

            // DocValues = true by default

            // keep store false for performance
            _dimField = new StringProperty { Index = FieldIndexOption.NotAnalyzed, Store = false };

            // todo Dims (in slicers) with high cardinality, only in groupby 
            //_dimDvField = new StringProperty { Index = FieldIndexOption.NotAnalyzed, Store = false, DocValues = true };

            // tpd DocValues cannot be used with analyzed strings
            _analyzedField = new StringProperty { Index = FieldIndexOption.Analyzed, Store = true, Analyzer = "snowball" };

            _dateField = new DateProperty { Index = NonStringIndexOption.No, Store = false };

            // todo memory fielddata
            _intField = new NumberProperty // as dimension. use fielddata
            { Index = NonStringIndexOption.No, Type = "integer", Store = false, IgnoreMalformed = true };

            _intMeasureField = new NumberProperty { Index = NonStringIndexOption.No, Type = "integer", Store = false, DocValues = true, IgnoreMalformed = true };

            _doubleMeasureField = new NumberProperty { Index = NonStringIndexOption.No, Type = "double", Store = false, DocValues = true, IgnoreMalformed = true };

            _booleanField = new BooleanProperty { Index = NonStringIndexOption.No, Store = false };
        }

        private void IndexData(int cubeId, string cubeName, FluentModel dbContext)
        {
            string sqlQuery = @"Select * from MeasureGroup where Active=1 and CubeId=" + cubeId;
            var measureGroups = dbContext.ExecuteQuery<MeasureGroup>(sqlQuery);
            var settings = GetIndexState();

            try
            {
                foreach (MeasureGroup measureGroup in measureGroups)
                {
                    string name = measureGroup.Name.ToLower();
                    bool createIndex = false;

                    if (_destination.Equals("Elastic", StringComparison.OrdinalIgnoreCase) || string.IsNullOrEmpty(_destination))
                    {
                        if (!_client.IndexExists(new IndexExistsRequest(name)).Exists)
                        {
                            createIndex = true;
                            var createResponse = _client.CreateIndex(name, i => i.InitializeUsing(settings));

                            if (!createResponse.IsValid)
                            {
                                _logger.Info("Error Creating Writing Index {0} Type {0}", name);
                                return;
                            }
                        }
                    }

                    DateTime startdate = DateTime.MinValue;

                    _stopWatch.Restart();
                    _logger.Info("Writing Index {0} into Elastic - Type {1}, Method: {2}", cubeName, name, _destination);

                    if (createIndex || (!string.IsNullOrEmpty(_destination) && _destination.Equals("MsSql", StringComparison.OrdinalIgnoreCase)))
                        startdate = measureGroup.MinDate ?? DateTime.UtcNow.AddYears(-2);
                    else
                    {
                        if (measureGroup.LastUpdate != null)
                            startdate = (DateTime)measureGroup.LastUpdate;
                    }

                    // load _sortedList
                    if (_destination.Equals("Elastic", StringComparison.OrdinalIgnoreCase) || string.IsNullOrEmpty(_destination))
                        InsertDynamicEs(cubeId, measureGroup.Id, name, measureGroup.NamedQuery, measureGroup.Measures, measureGroup.BusinessKey.Trim(), measureGroup.LongText, measureGroup.DateColumn.Trim(), startdate, createIndex, dbContext);
                    else
                        InsertDynamicSql(cubeId, measureGroup.NamedQuery, measureGroup.BusinessKey.Trim(), measureGroup.DateColumn.Trim(), startdate);


                    // tpt parallel?  done - chunk for bulk index
                    if (string.IsNullOrEmpty(measureGroup.BusinessKey))
                    {
                        foreach (var group in _dynList.Chunk(5000))
                        {
                            _client.IndexMany(group, name, name);
                        }

                        _stopWatch.Stop();
                        _logger.Info("Time taken {0} to write {1} rows", _stopWatch.Elapsed.TotalSeconds, _dynList.Count);
                        _dynList.Clear();
                    }
                    else
                    {
                        if (_destination.Equals("Elastic", StringComparison.OrdinalIgnoreCase) || string.IsNullOrEmpty(_destination))
                        {
                            foreach (var group in _sortedList.Values.Chunk(5000))
                            {
                                //_client.IndexMany(group, name, name);
                                // tpd skip set bizkey as _id #6730
                                //obj.Add("_id", bizkey);   FilterPath = measureGroup.BusinessKey.Trim()
                                var indexOps = group.Select(o => new BulkIndexOperation<dynamic>(o) { Index = name, Type = name }).Cast<IBulkOperation>().ToList(); // { Routing=mg.RoutingTerms} 
                                var indexResp = _client.Bulk(new BulkRequest { Operations = indexOps }); // url level
                                if (!indexResp.IsValid)
                                    _logger.Info("Error Creating Writing Index {0}, BusinessKey {1} ", name, measureGroup.BusinessKey);
                            }
                        }
                        else
                        {
                            InsertDataIntoSqlServer(_sortedList.Values);
                        }

                        _stopWatch.Stop();
                        _logger.Info("Time taken {0} to write {1} rows", _stopWatch.Elapsed.TotalSeconds, _sortedList.Values.Count);
                        _sortedList.Clear();
                    }

                    // todo? add async wait for ES to index
                    // wait for refresh_interval 1s
                    Thread.Sleep(1000);

                    // write lastUpdate
                    string dateColumn = measureGroup.DateColumn.Trim();

                    // tpt query using raw json
                    //var responseDate = _client.Search<dynamic>(s => s.Index(name).Type(name).Query(q=>q.Raw(json)));

                    // tpd error if Field is not matched - case sensitive
                    // todo catch exception

                    // tpt .Source(false) _sort is now garbage   
                    var searchDesc = new SearchDescriptor<dynamic>().Size(2).MatchAll().Sort(s => s.Field(
                        f=>f.Field(new Nest.Field { Name = dateColumn }).Mode(SortMode.Sum).Descending()));

                    string jsonStr = Encoding.UTF8.GetString(_client.Serializer.SerializeToBytes(searchDesc));
                    
                    var postData = new PostData<dynamic>(jsonStr);
                    var responseDate = _client.Raw.Search<dynamic>(name, name, postData);

                    if (responseDate.Success)
                    {
                        // tpd raw response parser
                        string respStr = Encoding.UTF8.GetString(responseDate.ResponseBodyInBytes);
                        var results = JsonObject.Parse(respStr);
                        string dateValue = results.Object("hits").ArrayObjects("hits").GetLastDate(dateColumn);

                        if (string.IsNullOrEmpty(dateValue)) {
                            _logger.Warn("Error getting DateColumn for lastupdate", measureGroup.DateColumn.Trim());
                        }
                        else
                        {
                            DateTime lastDate; // 01-01-0001 
                            DateTime.TryParse(dateValue, out lastDate);
                            if (lastDate.Year != 1)
                                measureGroup.LastUpdate = lastDate;
                            else
                            {
                                // tpt? try other date formats

                            }
                        }

                        dbContext.SaveChanges();
                    }
                    else
                    {
                        _logger.Error("Error searching DateColumn", measureGroup.DateColumn.Trim());
                    }

                    _dynList.Clear();
                    _sortedList.Clear();

                    // http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/indices-optimize.html
                    /*var update = _client.UpdateSettings(us => us.Index(name).RefreshInterval("5s"));
                    if (update.IsValid)
                    {
                        _client.Optimize(od => od.Index(name).MaxNumSegments(2));
                    }*/
                }
            }
            catch (Exception e)
            {
                _logger.Error(e.StackTrace);
            }

        }

        // add type mapping
        private IndexState GetIndexState()
        {
            var indexState = new IndexState();

            // todo for Archival https://www.elastic.co/blog/store-compression-in-lucene-and-elasticsearch
            // settings.Settings.Add("index.codec", "best_compression");   experimental  index-modules.html

            var settings = new IndexSettings {
                
                NumberOfShards = 2,
                NumberOfReplicas = 0,  // Defaults to 1
                //AutoExpandReplicas = "false", // Defaults to false

                // https://www.elastic.co/guide/en/elasticsearch/reference/current/index-modules-store.html
                FileSystemStorageImplementation = FileSystemStorageImplementation.MMap,  // default win x64, faster if have mega RAM / virtual mem
                // RefreshInterval = new Time(1000),  // Bug Defaults to 1s     -1 to disable refresh

                Analysis = new Analysis
                {
                    Analyzers = new Analyzers { { "snowball", new SnowballAnalyzer() } }  // { Language = SnowballLanguage.English } defaults to English
                }
                //{ "standard", new StandardAnalyzer { MaxTokenLength = 2 } }
            };

            // todo add Index Shard Allocation
            // tpt slow log settings

            indexState.Settings = settings;

            return indexState;
        }

        private void InsertDynamicEs(int cubeId, int measureGroupId, string measureGroup, string namedQuery, string measures, string bizKeyColumn,
            string longtext, string dateKeyColumn, DateTime startdate, bool createIndex, FluentModel dbContext)
        {
            SqlParameter startParam = new SqlParameter("@startdate", SqlDbType.DateTime) { Value = startdate };
            SqlParameter endParam = new SqlParameter("@enddate", SqlDbType.DateTime) { Value = DateTime.UtcNow.AddDays(1) };

            DataTable dataTable = DbConnector.GenerateDataTable(cubeId, namedQuery, startParam, endParam);
            Dictionary<string, string> typeDict = dataTable.Columns.Cast<DataColumn>().ToDictionary(c => c.ColumnName, c => c.DataType.FullName);

            _dateColumnList = new List<int>();
            int colNo = 0;

            if (createIndex)
            {
                //LoadDates(measureGroupId, dbContext);
                InsertMappings(measureGroupId, measureGroup, measures, bizKeyColumn, longtext, dbContext, typeDict, colNo);
            }
            else
            {
                var resp = _client.GetMapping<object>(m => m.Index(measureGroup));
                if (!resp.IsValid) // add type mapping if not exists
                {
                    _logger.Error("measureGroup Name : {0} with mapping not found in ES", measureGroup);
                    return;
                }
                else
                {
                    // todo Delta updates - populate typeDict from Mappings
                    // 
                }
            }

            // removed duplicates
            Dictionary<string, long> snapshotDict = null;
            if(!string.IsNullOrEmpty(bizKeyColumn))
                snapshotDict = new Dictionary<string, long>();

            // insert data row
            List<string> keys = typeDict.Keys.ToList();

            if (string.IsNullOrEmpty(bizKeyColumn))
            {
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    dynamic expando = new ExpandoObject();
                    var obj = (IDictionary<string, object>) expando;

                    for (int j = 0; j < dataTable.Columns.Count; j++)
                    {
                        if (dataTable.Rows[i][j].Equals(DBNull.Value))
                            continue;

                        if (_dateColumnList.Contains(j))
                        {
                            // add date-hier dim keys
                            DateTime date = (DateTime) dataTable.Rows[i][j];
                            obj.Add(keys[j], date);
                            obj.Add(keys[j] + "-Hour", date.Hour.ToString(CultureInfo.InvariantCulture));
                            obj.Add(keys[j] + "-Day", date.Day.ToString(CultureInfo.InvariantCulture));
                            obj.Add(keys[j] + "-DayOfWeek", ((int) date.DayOfWeek + 1).ToString(CultureInfo.InvariantCulture));
                            obj.Add(keys[j] + "-WeekOfYear", DateHelper.GetWeekOfYear(date).ToString(CultureInfo.InvariantCulture));
                            obj.Add(keys[j] + "-Quarter", DateHelper.GetQuarter(date).ToString(CultureInfo.InvariantCulture));
                        }
                        else
                            obj.Add(keys[j], dataTable.Rows[i][j]);
                    }
                    _dynList.Add(obj);
                }
            }
            else // remove duplicates as businesskey exists
            {
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    dynamic expando = new ExpandoObject();
                    var obj = (IDictionary<string, object>)expando;

                    // populate row obj
                    for (int j = 0; j < dataTable.Columns.Count; j++)
                    {
                        if (dataTable.Rows[i][j].Equals(DBNull.Value))
                            continue;

                        if (_dateColumnList.Contains(j))
                        {
                            // add date-hier dim keys
                            DateTime date = (DateTime)dataTable.Rows[i][j];
                            obj.Add(keys[j], date);
                            obj.Add(keys[j] + "-Hour", date.Hour.ToString(CultureInfo.InvariantCulture));
                            obj.Add(keys[j] + "-Day", date.Day.ToString(CultureInfo.InvariantCulture));
                            obj.Add(keys[j] + "-DayOfWeek", ((int)date.DayOfWeek + 1).ToString(CultureInfo.InvariantCulture));
                            obj.Add(keys[j] + "-WeekOfYear", DateHelper.GetWeekOfYear(date).ToString(CultureInfo.InvariantCulture));
                            obj.Add(keys[j] + "-Quarter", DateHelper.GetQuarter(date).ToString(CultureInfo.InvariantCulture));
                        }
                        else
                            obj.Add(keys[j], dataTable.Rows[i][j]);
                    }

                    object bizkeyObj = null;
                    string bizkey = "";

                    obj.TryGetValue(bizKeyColumn, out bizkeyObj);

                    // tpd fix Bug where IndexFactColumn won't match name as in SqlQuery
                    if (bizkeyObj == null)
                        obj.TryGetValue(bizKeyColumn.ToUpperFirstLetter(), out bizkeyObj); // auto add UpperFirstLetter to IndexFactColumn

                    if (bizkeyObj == null)
                        obj.TryGetValue(bizKeyColumn.ToLower(), out bizkeyObj);

                    if (bizkeyObj != null)
                        bizkey = bizkeyObj.ToString();

                    if (string.IsNullOrEmpty(bizkey))
                        _logger.Error("Null value in BusinessKey {0}", bizKeyColumn);

                    object dateKeyObj;
                    obj.TryGetValue(dateKeyColumn, out dateKeyObj);

                    if (dateKeyObj == null)
                        obj.TryGetValue(dateKeyColumn.ToUpperFirstLetter(), out dateKeyObj); // auto add UpperFirstLetter to IndexFactColumn

                    if (dateKeyObj == null)
                        obj.TryGetValue(dateKeyColumn.ToLower(), out dateKeyObj);

                    if (dateKeyObj == null)
                    {
                        _logger.Error("Null value in DateColumn: {0}", dateKeyColumn);
                    }
                    else
                    {
                        if (snapshotDict.ContainsKey(bizkey))
                        {
                            //if (bizkey.Contains("CAPINC0000070")) _sortedList.Remove("");

                            // only add if new row is later than snapshotDict long
                            if (((DateTime)dateKeyObj).Ticks > snapshotDict[bizkey])
                            {
                                _sortedList.Remove(bizkey);
                                _sortedList.Add(bizkey, obj);

                                snapshotDict[bizkey] = ((DateTime)dateKeyObj).Ticks;
                            }
                        }
                        else // add new ticks value
                        {
                            snapshotDict.Add(bizkey, ((DateTime)dateKeyObj).Ticks);
                            _sortedList.Add(bizkey, obj);
                        }
                    }
                }
            }
        }

        private void InsertMappings(int measureGroupId, string measureGroup, string measures, string businesskey, string longtext, FluentModel dbContext, Dictionary<string, string> typeDict, int colNo)
        {
            var properties = new Dictionary<PropertyName, IProperty>();

            // tpt add meta data? 
            /*FluentDictionary<string, object> fdict = null;
            if (!string.IsNullOrEmpty(businesskey))
            {
                fdict = new FluentDictionary<string, object>();
                fdict.Add("_id", _dimField);
                // properties.Add("id", _dimField);
            }*/

            foreach (string key in typeDict.Keys)
            {
                if (typeDict[key].Contains("Double") || typeDict[key].Contains("Decimal")) // write to Measures if Double
                {
                    // add as measure
                    properties.Add(key, _doubleMeasureField);
                    dbContext.Add(NewMeasure(measureGroupId, key));
                }
                else if (typeDict[key].Contains("Int"))
                {
                    if (!string.IsNullOrEmpty(measures) && measures.IndexOf(key, StringComparison.OrdinalIgnoreCase) >= 0) // for int measure
                    {
                        properties.Add(key, _intMeasureField);
                        dbContext.Add(NewMeasure(measureGroupId, key));
                    }
                    else
                    {
                        properties.Add(key, _intField);
                        dbContext.Add(CreateField(measureGroupId, key, "System.Int32"));
                    }
                }
                else if (typeDict[key].Contains("Bool"))
                {
                    properties.Add(key, _booleanField);
                    dbContext.Add(CreateField(measureGroupId, key, "System.Boolean"));
                }
                else if (typeDict[key].Contains("DateTime"))
                {
                    _dateColumnList.Add(colNo + _dateColumnList.Count * 5); //  
                    AddDateKeys(properties, key, measureGroupId, dbContext);
                }
                else // string
                {
                    // todo read config for searchable long text / smart rule?
                    if (string.IsNullOrEmpty(longtext))
                        properties.Add(key, _dimField);
                    else
                        properties.Add(key, longtext.IndexOf(key, StringComparison.OrdinalIgnoreCase) >= 0 ? _analyzedField : _dimField);

                    dbContext.Add(CreateField(measureGroupId, key, "System.String"));
                }

                colNo++;
            }
            dbContext.SaveChanges();

            // ES _source is true for DT, _all is false
            // todo scenario where source is false?

            var putMapping = new PutMappingRequest(measureGroup, measureGroup)
            {
                Properties = new Properties(properties),
                Dynamic = DynamicMapping.Ignore,
                //SourceField = new SourceField { Enabled = true }, //Compress = true
                AllField = new AllField { Enabled = false }
                //Meta = fdict  
            };

            IIndicesResponse response = _client.Map(putMapping);

            if (response.IsValid)
                _logger.Info("Initial Mapping for index {0} is complete", measureGroup);
            else
                _logger.Error("Mapping failed for index {0}", measureGroup);
        }

        #region internal helpers
        private void LoadDates(int measureGroupId, FluentModel dbContext)
        {
            //_keyDates = null;
            IQueryable<MeasureGroup> mGroups = from mg in dbContext.MeasureGroups where mg.Id == measureGroupId
                                               select mg;

            var firstOrDefault = mGroups.FirstOrDefault();
            //if (firstOrDefault != null) _keyDates = firstOrDefault.DateColumn.Trim();
        }

        /*// use DateColumn from MeasureGroup
        private bool IsKeyDateDimension(string key) 
        {
            string[] arr = _keyDates.Split(',');

            for (int i = 0; i < arr.Length; i++)
            {
                if (key.Equals(arr[i]))
                    return true;
            }
            return false;
        }*/

        private void AddDateKeys(Dictionary<PropertyName, IProperty> properties, string key, int measureGroupId, FluentModel dbContext) 
        {
            properties.Add(key, _dateField);
            // add to dict for indexing, all strings
            properties.Add(key + "-Hour", _dimField);
            properties.Add(key + "-Day", _dimField);
            properties.Add(key + "-DayOfWeek", _dimField);
            properties.Add(key + "-WeekOfYear", _dimField);
            properties.Add(key + "-Quarter", _dimField);  // school term?
            //properties.Add(key + "-Month", _dimField);

            dbContext.Add(new Field { MeasureGroupId = measureGroupId, Name = key, Caption = key, Type = "System.DateTime" });
            dbContext.Add(new Field { MeasureGroupId = measureGroupId, Name = key + "-Hour", Caption = "Hour", Type = "System.DateTime" });
            dbContext.Add(new Field { MeasureGroupId = measureGroupId, Name = key + "-Day", Caption = "Day", Type = "System.DateTime" });
            dbContext.Add(new Field { MeasureGroupId = measureGroupId, Name = key + "-DayOfWeek", Caption = "Day of Week", Type = "System.DateTime" });
            dbContext.Add(new Field { MeasureGroupId = measureGroupId, Name = key + "-WeekOfYear", Caption = "Week of Year", Type = "System.DateTime" });
            dbContext.Add(new Field { MeasureGroupId = measureGroupId, Name = key + "-Quarter", Caption = "Quarter", Type = "System.DateTime" });
            //dbContext.Add(new Field { MeasureGroupId = measureGroupId, Name = key + "-Month", Caption = "Month", Type = "System.DateTime" });
        }

        // add to Hierachies for date dim
        private void InsertDateHierarchies(IEnumerable<string> dateKeys, int measureGroupId, FluentModel dbContext)
        {
            foreach (string key in dateKeys)
            {
                IQueryable<Field> fields = from f in dbContext.Fields
                                           where f.Name.Equals(key) && f.MeasureGroupId == measureGroupId
                                           select f;

                var field = fields.FirstOrDefault();
                if (field != null)
                {
                    int parentId = field.Id;

                    dbContext.Add(new Hierarchy { FieldId = parentId, ParentId = parentId, Caption = "Date" });
                    dbContext.Add(new Hierarchy { FieldId = parentId, ParentId = parentId, Caption = "Day Of Week" });
                    dbContext.Add(new Hierarchy { FieldId = parentId, ParentId = parentId, Caption = "Week And Year" });
                    dbContext.Add(new Hierarchy { FieldId = parentId, ParentId = parentId, Caption = "Month And Year" });
                    dbContext.Add(new Hierarchy { FieldId = parentId, ParentId = parentId, Caption = "Year" });
                }
            }
            dbContext.SaveChanges();
        }


        private Measure NewMeasure(int measureGroupId, string name)
        {
            Measure ms = new Measure
            {
                MeasureGroupId = measureGroupId,
                Name = name,
                Caption = name,
                UniqueName = "[Measures].[" + name + "]",
                Aggregator = 1, // sum
                DisplayFolder = "Default",
                Visible = true
            };
            return ms;
        }

        private Field CreateField(int measureGroupId, string key, string type)
        {
            Field dim = new Field
            {
                MeasureGroupId = measureGroupId,
                Name = key,
                Caption = key,
                Type = type
                //UniqueName = "[" + key + "]",
            };

            return dim;
        }

        #endregion


        private void InsertDynamicSql(int cubeId, string namedQuery, string businesskey, string dateKeyColumn, DateTime startdate)
        {
            SqlParameter startParam = new SqlParameter("@startdate", SqlDbType.DateTime) {Value = startdate};
            SqlParameter endParam = new SqlParameter("@enddate", SqlDbType.DateTime) {Value = DateTime.UtcNow.AddDays(1)};

            DataTable dataTableSource = DbConnector.GenerateDataTable(cubeId, namedQuery, startParam, endParam);

            _dateColumnList = new List<int>();
            _columnTypeList = new List<Type>();

            for (int i = 0; i < dataTableSource.Columns.Count; i++)
            {
                _columnTypeList.Add(dataTableSource.Columns[i].DataType);

                if (dataTableSource.Columns[i].ColumnName.Equals(dateKeyColumn, StringComparison.OrdinalIgnoreCase))
                    _dateColumnList.Add(i);
            }

            // removed duplicates
            Dictionary<string, long> snapshotDict = new Dictionary<string, long>();

            //DataTable dataTable 
            for (int i = 0; i < dataTableSource.Rows.Count; i++)
            {
                dynamic expando = new ExpandoObject();
                var obj = (IDictionary<string, object>)expando;

                // populate row obj
                for (int j = 0; j < dataTableSource.Columns.Count; j++)
                {
                    obj.Add(dataTableSource.Columns[j].ColumnName, dataTableSource.Rows[i][j]); 
                }

                object bizkeyObj = null;
                obj.TryGetValue(businesskey, out bizkeyObj);

                string bizkey = "";
                if (bizkeyObj != null)
                    bizkey = bizkeyObj.ToString();

                object dateObj;
                obj.TryGetValue(dateKeyColumn, out dateObj);

                if (dateObj == null)
                    _logger.Info("null value in dateKeyColumn: {0} for value: {1}", dateKeyColumn, bizkey);

                if (snapshotDict.ContainsKey(bizkey))
                {
                    // only add if new row is later than snapshotDict long
                    if (((DateTime) dateObj).Ticks > snapshotDict[bizkey])
                    {
                        _sortedList.Remove(bizkey);
                        _sortedList.Add(bizkey, obj);

                        snapshotDict[bizkey] = ((DateTime) dateObj).Ticks;
                    }
                }
                else // add new ticks value
                {
                    snapshotDict.Add(bizkey, ((DateTime) dateObj).Ticks);
                    _sortedList.Add(bizkey, obj);
                }
            }
        }

        private void InsertDataIntoSqlServer(IEnumerable<dynamic> data)
        {
            try
            {
                using (var connection = new SqlConnection("Server=.;Database=Ge;Integrated Security=SSPI"))
                {
                    connection.Open();

                    // Measure
                    var stopwatch = new Stopwatch();
                    stopwatch.Start();

                    DataTable dataTable = new DataTable();

                    int index = 0;
                    var obj1 = (IDictionary<string, object>)data.ElementAt(0);
                    foreach (string key in obj1.Keys)
                    {
                        dataTable.Columns.Add(key, _columnTypeList.ElementAt(index));  // .Add("TI", typeof(Int32));
                        index++;
                    }

                    foreach (var dataObj in data)
                    {
                        var obj = (IDictionary<string, object>)dataObj;
                        string key;

                        DataRow row = dataTable.NewRow();
                        for (int i = 0; i < dataTable.Columns.Count; i++)
                        {
                            key = obj.Keys.ElementAt(i);
                            object value = null;
                            obj.TryGetValue(key, out value);
                            row[i] = value;
                        }
                        dataTable.Rows.Add(row);
                    }

                    var bulkCopy = new SqlBulkCopy(connection) { DestinationTableName = "Incident" };
                    bulkCopy.WriteToServer(dataTable); // dataReader

                    /*var obj = (IDictionary<string, object>)data.ElementAt(0);
                    foreach (string key in obj.Keys)
                    {
                        bulkCopy.ColumnMappings.Add(key, key);
                    }

                    using (var dataReader = new ObjectDataReader<dynamic>(data))
                    {
                        bulkCopy.WriteToServer(dataReader);
                    }*/

                    _logger.Info("Bulk copy: {0}ms", stopwatch.ElapsedMilliseconds);
                }
            }
            catch (Exception e)
            {
                _logger.Error("Exception at InsertDataUsingSqlBulkCopy: {0}", e.StackTrace);
            }
            
        }
    }
}
