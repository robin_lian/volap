﻿using System.Collections.Generic;
using Volap.Data.Core.Interfaces;

namespace Volap.Data.Core.Base
{
    /// <summary>
    ///     The Cube class represents an OLAP cube that holds a multi dimensional representation of a flat data set.
    ///     A cube is analogous to a table in a relational database. Where a table in a relational database has two dimensions,
    ///     a cube can have any number of dimensions. In its simplest form, the dimensions of a cube correspond to a field of
    ///     the flat data set.
    /// </summary>
    public class Cube : ICube
    {
        #region Members

        private readonly string _caption;
        private readonly List<IDimension> _dimensions = new List<IDimension>();
        private readonly List<IMeasureGroup> _measureGroups = new List<IMeasureGroup>();
        private readonly List<IMeasure> _measures = new List<IMeasure>();

        #endregion // Members

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="Cube" /> class.
        /// </summary>
        /// <param name="name">The name of the cube.</param>
        /// <param name="catalogName">The name of the catalog this belongs to.</param>
        /// <param name="caption">The caption of the cube.</param>
        /// <param name="description">The description of the cube.</param>
        public Cube(string name, string catalogName, string caption, string description)
        {
            Name = name;
            CatalogName = catalogName;
            _caption = caption;
            Description = description;
            Dimensions = new OlapElementCollection<IDimension>(_dimensions);
            Measures = new OlapElementCollection<IMeasure>(_measures);
            MeasureGroups = new OlapElementCollection<IMeasureGroup>(_measureGroups);
            MeasureGroupDimensions = new Dictionary<IMeasureGroup, List<IDimension>>();
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets a value indicating whether <see cref="Measures" /> collections is loaded.
        /// </summary>
        public bool MeasuresLoaded { get; internal set; }

        /// <summary>
        ///     Gets a value indicating whether <see cref="Dimensions" /> collections is loaded.
        /// </summary>
        public bool DimensionsLoaded { get; internal set; }

        /// <summary>
        ///     Gets a value indicating whether <see cref="MeasureGroups" /> collections is loaded.
        /// </summary>
        public bool MeasureGroupsLoaded { get; internal set; }

        /// <summary>
        ///     Gets a value indicating whether <see cref="MeasureGroupDimensions" /> collections is loaded.
        /// </summary>
        public bool MeasureGroupDimensionsLoaded { get; internal set; }

        #region Implementation of ICube

        /// <summary>
        ///     Gets the name of the catalog this cube belongs to.
        /// </summary>
        public string CatalogName { get; private set; }

        /// <summary>
        ///     Gets the name of the cube.
        /// </summary>
        /// <value>The name of the Cube.</value>
        public string Name { get; private set; }

        /// <summary>
        ///     Gets the unique name of the element.
        /// </summary>
        /// <value>The unique name of the element.</value>
        public string UniqueName
        {
            get { return Name; }
        }

        /// <summary>
        ///     Gets the caption of the cube.
        /// </summary>
        /// <value>The caption.</value>
        public string Caption
        {
            get
            {
                if (_caption == null)
                {
                    return Name;
                }

                return _caption;
            }
        }

        /// <summary>
        ///     Gets the description of the cube.
        /// </summary>
        /// <value>The description.</value>
        public string Description { get; private set; }

        /// <summary>
        ///     Gets a ReadOnlyCollection with the measures that are defined for the cube.
        /// </summary>
        /// <value>The measures.</value>
        public OlapElementCollection<IMeasure> Measures { get; private set; }

        /// <summary>
        ///     Gets a ReadOnlyCollection with the dimensions that are defined for the cube.
        /// </summary>
        /// <value>The dimensions.</value>
        public OlapElementCollection<IDimension> Dimensions { get; private set; }

        /// <summary>
        ///     Gets a ReadOnlyCollection with the measure groups that are defined for the cube.
        /// </summary>
        /// <value>The dimensions.</value>
        public OlapElementCollection<IMeasureGroup> MeasureGroups { get; private set; }

        /// <summary>
        ///     Gets dictionary with dimensions for given measure group.
        /// </summary>
        public Dictionary<IMeasureGroup, List<IDimension>> MeasureGroupDimensions { get; private set; }

        /// <summary>
        ///     Gets a measure dimension of a cube.
        /// </summary>
        public IDimension MeasureDimension { get; protected internal set; }

        #endregion

        #region Internal

        internal List<IMeasure> MeasuresCollection
        {
            get { return _measures; }
        }

        internal List<IDimension> DimensionsCollection
        {
            get { return _dimensions; }
        }

        internal List<IMeasureGroup> MeasureGroupsCollection
        {
            get { return _measureGroups; }
        }

        #endregion // Internal

        #endregion // Properties
    }
}