﻿using System.Collections.Generic;
using Volap.Data.Core.Interfaces;

namespace Volap.Data.Core.Base
{
    /// <summary>
    /// Represents a dimension within a cube
    /// </summary>
    public class Dimension : IDimension
    {
        #region Members

        private readonly List<IHierarchy> _hierarchies = new List<IHierarchy>();

        #endregion // Members

        #region Constructor

        public Dimension(string name)
        {
            Name = name;
            UniqueName = "[" + name + "]";
            Hierarchies = new OlapElementCollection<IHierarchy>(_hierarchies);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Dimension"/> class.
        /// </summary>
        /// <param name="name">The name of the dimension.</param>
        /// <param name="uniqueName">The unique name of the dimension.</param>
        /// <param name="caption">The caption of the dimension.</param>
        /// <param name="description">The description of the dimension.</param>
        public Dimension(string name, string uniqueName, string caption, string description)
        {
            Name = name;
            UniqueName = uniqueName;
            Caption = caption;
            Description = description;
            Hierarchies = new OlapElementCollection<IHierarchy>(_hierarchies);
        }

        #endregion // Constructor

        #region Properties

        #region Implementation of IDimension

        /// <summary>
        /// Gets the name of the dimension.
        /// </summary>
        /// <value>The name of the dimension.</value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the unique name of the dimension.
        /// </summary>
        /// <value>The unique name of the dimension.</value>
        public string UniqueName { get; private set; }

        /// <summary>
        /// Gets the caption of the dimension.
        /// </summary>
        /// <value>The caption.</value>
        public string Caption { get; private set; }

        /// <summary>
        /// Gets the description of the dimension.
        /// </summary>
        /// <value>The description.</value>
        public string Description { get; private set; }

        /// <summary>
        /// Gets a ReadOnlyCollection with the hierarchies within the dimension.
        /// </summary>
        /// <value>Hierarchies within the dimension.</value>
        public OlapElementCollection<IHierarchy> Hierarchies { get; private set; }

        #endregion

        #region Internal

        internal List<IHierarchy> HierarchiesCollection
        {
            get { return _hierarchies; }
        }

        #endregion // Internal

        #endregion // Properties
    }
}
