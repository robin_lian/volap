﻿using System.Collections.Generic;
using Volap.Data.Core.Interfaces;

namespace Volap.Data.Core.Base
{
    /// <summary>
    ///     Represents a hierarchy within a dimension
    /// </summary>
    public class Hierarchy : IHierarchy
    {
        #region Members

        private readonly List<ILevel> _levels = new List<ILevel>();

        #endregion // Members

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="Hierarchy" /> class.
        /// </summary>
        /// <remarks> Use it only for serialization. </remarks>
        public Hierarchy()
        {
        }

        public Hierarchy(string name, string uniqueName) {
            Name = name;
            UniqueName = uniqueName;
            Levels = new OlapElementCollection<ILevel>(_levels);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Hierarchy" /> class.
        /// </summary>
        /// <param name="name">The name of the hierarchy.</param>
        /// <param name="uniqueName">The unique name of the hierarchy.</param>
        /// <param name="caption">The caption of the hierarchy.</param>
        /// <param name="description">The description of the hierarchy.</param>
        /// <param name="defaultMember">The unique name of the default member for the Hierarchy.</param>
        /// <param name="allMember">The unique name of the "All" member for the Hierarchy.</param>
        /// <param name="parentDimension">The parent dimension.</param>
        /// <param name="groupName">The name of the group this hierarchy belongs to.</param>
        /// <param name="hierarchyOrigin">The source of the hierarchy.</param>
        public Hierarchy(string name, string uniqueName, string caption, string description, string defaultMember, string allMember, IDimension parentDimension, string groupName, HierarchyOrigin hierarchyOrigin)
        {
            Name = name;
            UniqueName = uniqueName;
            Caption = caption;
            Description = description;
            DefaultMember = defaultMember;
            AllMember = allMember;
            ParentDimension = parentDimension;
            GroupName = groupName;
            HierarchyOrigin = hierarchyOrigin;
            Levels = new OlapElementCollection<ILevel>(_levels);
            //this.IsLoaded = false;
        }

        #endregion // Constructor

        #region Properties

        #region Implementation of IHierarchy

        /// <summary>
        ///     Gets the name of the hierarchy.
        /// </summary>
        /// <value>The name of the hierarchy.</value>
        public string Name { get; private set; }

        /// <summary>
        ///     Gets the unique name of the hierarchy.
        /// </summary>
        /// <value>The unique name of the hierarchy.</value>
        public string UniqueName { get; private set; }

        /// <summary>
        ///     Gets the caption of the hierarchy.
        /// </summary>
        /// <value>The caption.</value>
        public string Caption { get; private set; }

        /// <summary>
        ///     Gets the description of the hierarchy.
        /// </summary>
        /// <value>The description.</value>
        public string Description { get; private set; }

        /// <summary>
        ///     Gets the unique name of the default member for the Hierarchy
        /// </summary>
        /// <value>The unique name of the default member for the Hierarchy.</value>
        public string DefaultMember { get; private set; }

        /// <summary>
        ///     Gets the "All" member of this hierarchy.
        /// </summary>
        /// <value>The "All" member.</value>
        public string AllMember { get; private set; }

        /// <summary>
        ///     Gets the dimension that contains the hierarchy.
        /// </summary>
        /// <value>The parent dimension.</value>
        public IDimension ParentDimension { get; private set; }

        /// <summary>
        ///     Gets a ReadOnlyCollection with the levels within the hierarchy.
        /// </summary>
        /// <value>Levels within the hierarchy.</value>
        public OlapElementCollection<ILevel> Levels { get; private set; }

        /// <summary>
        ///     Gets the name of the measue group hierarachy belongs to.
        /// </summary>
        public string GroupName { get; private set; }

        /// <summary>
        ///     Gets the source of the hierarchy.
        /// </summary>
        public HierarchyOrigin HierarchyOrigin { get; private set; }

        #endregion // Implementation of IHierarchy

        /// <summary>
        ///     Gets a value indicating whether all levels of this instance are loaded.
        /// </summary>
        /// <value><c>true</c> if all members are loaded; otherwise, <c>false</c>.</value>
/*        public bool IsLoaded
        {
            get
            {
                return this.Levels.Where(l => ((Level) l).IsLoaded).Count() == this.LevelsCollection.Count;
            }
        }*/

        #region Internal
        internal List<ILevel> LevelsCollection
        {
            get { return _levels; }
        }

        #endregion // Internal

        #endregion Properties
    }
}