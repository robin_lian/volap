namespace Volap.Data.Core.Base
{
    /// <summary>
    /// Represents a key performance indicator of the cube
    /// </summary>
    public class KeyPerformanceIndicator
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyPerformanceIndicator"/> class.
        /// </summary>
        /// <param name="name">The name of the <see cref="KeyPerformanceIndicator"/>.</param>
        /// <param name="caption">The caption of the <see cref="KeyPerformanceIndicator"/>.</param>
        /// <param name="description">The description of the <see cref="KeyPerformanceIndicator"/>.</param>
        /// <param name="measureGroupName">Name of the associated measure group.</param>
        /// <param name="displayFolder">The display folder.</param>
        /// <param name="value">The unique name of the member in the measures dimension for the KPI Value.</param>
        /// <param name="goal">The unique name of the member in the measures dimension for the KPI Goal. NULL if there is no goal defined.</param>
        /// <param name="status">The unique name of the member in the measures dimension for the KPI Status. NULL if there is no status defined.</param>
        /// <param name="trend">The unique name of the member in the measures dimension for the KPI Trend. NULL if there is no trend defined.</param>
        /// <param name="weight">The unique name of the member in the measures dimension for the KPI Weight. NULL if there is no weight defined.</param>
        /// <param name="statusGraphic">The default graphical representation of the <see cref="KeyPerformanceIndicator"/> for Status</param>
        /// <param name="trendGraphic">The default graphical representation of the <see cref="KeyPerformanceIndicator"/> for Trend</param>
        /// <param name="currentTime">The unique name of the member in the time dimension that defines the temporal context of the <see cref="KeyPerformanceIndicator"/>. NULL if there is no Time defined.</param>
        /// <param name="parentKeyPerformanceIndicatorName">Name of the parent key performance indicator.</param>
        /// <param name="scope">The scope of the <see cref="KeyPerformanceIndicator"/>.</param>
        /// <param name="annotations">A set of notes, in XML format.</param>
        public KeyPerformanceIndicator(string name, string caption, string description, string measureGroupName, string displayFolder, string value, string goal, string status, string trend, string weight, string statusGraphic, string trendGraphic, string currentTime, string parentKeyPerformanceIndicatorName, ScopeType scope, string annotations)
        {
            Name = name;
            Caption = caption;
            Description = description;
            MeasureGroupName = measureGroupName;
            DisplayFolder = displayFolder;
            Value = value;
            Goal = goal;
            Status = status;
            Trend = trend;
            Weight = weight;
            StatusGraphic = statusGraphic;
            TrendGraphic = trendGraphic;
            CurrentTime = currentTime;
            ParentKeyPerformanceIndicatorName = parentKeyPerformanceIndicatorName;
            Scope = scope;
            Annotations = annotations;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the name of the <see cref="KeyPerformanceIndicator"/>.
        /// </summary>
        /// <value>The name of the <see cref="KeyPerformanceIndicator"/>.</value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the caption associated with the <see cref="KeyPerformanceIndicator"/>.
        /// </summary>
        /// <value>The caption associated with the <see cref="KeyPerformanceIndicator"/>.</value>
        public string Caption { get; private set; }

        /// <summary>
        /// Gets the description of the <see cref="KeyPerformanceIndicator"/>.
        /// </summary>
        /// <value>The description of the <see cref="KeyPerformanceIndicator"/>.</value>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the associated measure group for the KPI.
        /// You can use this property to determine the dimensionality of the KPI. If null, the KPI will be dimensioned by all measure groups. 
        /// The default value is null. 
        /// </summary>
        /// <value>The associated measure group for the KPI.</value>
        public string MeasureGroupName { get; private set; }

        /// <summary>
        /// Gets a string that identifies the path of the display folder that the client application 
        /// uses to show the member. The folder level separator is defined by the client application. 
        /// For the tools and clients supplied by Analysis Services, the backslash (\) is the level separator. 
        /// To provide multiple display folders, use a semicolon (;) to separate the folders.
        /// </summary>
        /// <value>The display folder.</value>
        public string DisplayFolder { get; private set; }

        /// <summary>
        /// Gets the unique name of the member in the measures dimension.
        /// </summary>
        /// <value>The unique name of the member in the measures dimension.</value>
        public string Value { get; private set; }

        /// <summary>
        /// Gets the unique name of the member in the measures dimension. Returns NULL if there is no Goal defined.
        /// </summary>
        /// <value>The unique name of the member in the measures dimension. NULL if there is no Goal defined.</value>
        public string Goal { get; private set; }

        /// <summary>
        /// Gets the unique name of the member in the measures dimension. Returns NULL if there is no Status defined.
        /// </summary>
        /// <value>The unique name of the member in the measures dimension. NULL if there is no Status defined.</value>
        public string Status { get; private set; }

        /// <summary>
        /// Gets the unique name of the member in the measures dimension. Returns NULL if there is no Trend defined.
        /// </summary>
        /// <value>The unique name of the member in the measures dimension. NULL if there is no Trend defined.</value>
        public string Trend { get; private set; }

        /// <summary>
        /// Gets the unique name of the member in the measures dimension. Returns NULL if there is no Weight defined.
        /// </summary>
        /// <value>The unique name of the member in the measures dimension. NULL if there is no Weight defined.</value>
        public string Weight { get; private set; }

        /// <summary>
        /// Gets the default graphical representation of the <see cref="KeyPerformanceIndicator"/>.
        /// </summary>
        /// <value>The default graphical representation of the <see cref="KeyPerformanceIndicator"/>.</value>
        public string StatusGraphic { get; private set; }

        /// <summary>
        /// Gets the default graphical representation of the <see cref="KeyPerformanceIndicator"/>.
        /// </summary>
        /// <value>The default graphical representation of the <see cref="KeyPerformanceIndicator"/>.</value>
        public string TrendGraphic { get; private set; }

        /// <summary>
        /// Gets the unique name of the member in the time dimension that defines the temporal context of the <see cref="KeyPerformanceIndicator"/>. Returns NULL if there is no Time defined.
        /// </summary>
        /// <value>The unique name of the member in the time dimension that defines the temporal context of the <see cref="KeyPerformanceIndicator"/>. NULL if there is no Time defined.</value>
        public string CurrentTime { get; private set; }

        /// <summary>
        /// Gets the name of the parent <see cref="KeyPerformanceIndicator"/>.
        /// </summary>
        /// <value>The name of the parent <see cref="KeyPerformanceIndicator"/>.</value>
        public string ParentKeyPerformanceIndicatorName { get; private set; }

        /// <summary>
        /// Gets the scope of the KPI. The KPI can be a session KPI or global KPI.
        /// </summary>
        /// <value>The scope of the KPI.</value>
        public ScopeType Scope { get; private set; }

        /// <summary>
        /// Gets the annotations.
        /// </summary>
        /// <value>A set of notes, in XML format.</value>
        public string Annotations { get; private set; }

        #endregion // Properties
    }
}
