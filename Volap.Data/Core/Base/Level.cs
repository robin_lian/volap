﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Volap.Data.Core.Interfaces;

namespace Volap.Data.Core.Base
{
    /// <summary>
    ///     Represents a level within a hierarchy
    /// </summary>
    public class Level : ILevel
    {
        //private readonly List<IMember> _members = new List<IMember>();
        private readonly Dictionary<string, IMember> _memberNames;
        private readonly ObservableCollection<IMember> _members = new ObservableCollection<IMember>();
        //private bool _isLoaded;

        public Level(string name, string uniqueName)
        {
            Name = name;
            UniqueName = uniqueName;
            Members = new OlapElementCollection<IMember>(_members);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Level" /> class.
        /// </summary>
        /// <param name="name">The name of the level.</param>
        /// <param name="caption">The caption of the level.</param>
        /// <param name="uniqueName">The unique name of the level.</param>
        /// <param name="description">The description.</param>
        /// <param name="depth">The distance of the level from the root of the level. Root level is zero (0)</param>
        /// <param name="parentHierarchy">The parent hierarchy.</param>
        public Level(string name, string caption, string uniqueName, string description, int depth, IHierarchy parentHierarchy)
        {
            Name = name;
            Caption = caption;
            UniqueName = uniqueName;
            Description = description;
            Depth = depth;
            ParentHierarchy = parentHierarchy;
            Members = new OlapElementCollection<IMember>(_members);

            _memberNames = new Dictionary<string, IMember>();
            //IsLoaded = false;

            // TK - jquery
            _members.CollectionChanged += (s, e) =>
            {
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        foreach (IMember item in e.NewItems)
                        {
                            _memberNames.Add(item.UniqueName, item);
                        }
                        break;
                    case NotifyCollectionChangedAction.Remove:
                        foreach (IMember item in e.OldItems)
                        {
                            _memberNames.Remove(item.UniqueName);
                        }
                        break;

                    case NotifyCollectionChangedAction.Replace:

                        break;
                    case NotifyCollectionChangedAction.Reset:
                        _memberNames.Clear();
                        break;
                }
            };
        }

        #region Properties

        #region Implementation of ILevel

        /// <summary>
        ///     Gets the name of the level.
        /// </summary>
        /// <value>The name of the level.</value>
        public string Name { get; private set; }

        /// <summary>
        ///     Gets the caption of the level.
        /// </summary>
        /// <value>The caption.</value>
        public string Caption { get; private set; }

        /// <summary>
        ///     Gets the unique name of the level.
        /// </summary>
        /// <value>The unique name of the level.</value>
        public string UniqueName { get; private set; }

        /// <summary>
        ///     Gets the description of the level.
        /// </summary>
        /// <value>The description.</value>
        public string Description { get; private set; }

        /// <summary>
        ///     Gets the distance of the level from the root of the level. Root level is zero (0)
        /// </summary>
        /// <value>The depth.</value>
        public int Depth { get; private set; }

        /// <summary>
        ///     Gets the hierarchy that contains the level.
        /// </summary>
        /// <value>The parent hierarchy.</value>
        public IHierarchy ParentHierarchy { get; private set; }

        /// <summary>
        ///     Gets a ReadOnlyCollection with the members within the level.
        /// </summary>
        /// <value>Members within the level.</value>
        public OlapElementCollection<IMember> Members { get; private set; }

        #endregion // Implementation of ILevel

        /// <summary>
        ///     Gets a value indicating whether all members of this instance are loaded.
        /// </summary>
        /// <value><c>true</c> if all members are loaded; otherwise, <c>false</c>.</value>
/*        public bool IsLoaded
        {
            get
            {
                return this._isLoaded || 
                    this.MembersCount == this.MembersCollection.Count;
            }

            protected internal set { this._isLoaded = value; }
        }*/

        #region Internal
        internal ObservableCollection<IMember> MembersCollection
        {
            get { return _members; }
        }

        /// <summary>
        ///     Gets or sets the count of all members in the level.
        /// </summary>
        /// <value>The members count.</value>
        public int MembersCount { get; protected internal set; }

        #endregion // Internal

        #endregion Properties

        /// <summary>
        ///     Gets the member associated with the specified name.
        /// </summary>
        /// <param name="memberName">The member unique name.</param>
        /// <param name="member">The member.</param>
        /// <returns></returns>
        public bool TryGetMember(string memberName, out IMember member)
        {
            return _memberNames.TryGetValue(memberName, out member);
        }

        internal void AddMember(IMember member)
        {
            lock (MembersCollection)
            {
                if (_memberNames.ContainsKey(member.UniqueName))
                {
                    return;
                }

                MembersCollection.Add(member);
            }
        }
    }
}