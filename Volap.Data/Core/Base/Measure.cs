﻿using Volap.Data.Core.Interfaces;

namespace Volap.Data.Core.Base
{
    /// <summary>
    ///     Represents a measure within a cube
    /// </summary>
    public class Measure : IMeasure //, IOlapElement
    {
        private readonly int _hashCode;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Measure" /> class.
        /// </summary>
        /// <remarks> Use it only for serialization. </remarks>
        public Measure()
        {
        }

        public Measure(string name, string uniqueName)
        {
            Name = name;
            UniqueName = uniqueName;
            Caption = name;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Measure" /> class.
        /// </summary>
        /// <param name="name">The name of the measure.</param>
        /// <param name="uniqueName">The unique name of the measure.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="description">The description.</param>
        /// <param name="groupName">The name of the group this measure belongs to.</param>
        /// <param name="aggregatorType">The <see cref="AggregatorType" /> that identifies how a measure was derived.</param>
        public Measure(
            string name,
            string uniqueName,
            string caption,
            string description,
            string groupName,
            AggregatorType aggregatorType)
        {
            Name = name;
            UniqueName = uniqueName;
            Caption = caption;
            Description = description;
            GroupName = groupName;
            AggregatorType = aggregatorType;
            _hashCode = uniqueName.GetHashCode();
        }

        #region Properties

        /// <summary>
        ///     Gets or sets the measure display folder.
        /// </summary>
        public string MeasureDisplayFolder { get; protected internal set; }

        /// <summary>
        ///     Gets the name of the measure.
        /// </summary>
        /// <value>The name of the measure.</value>
        public string Name { get; private set; }

        /// <summary>
        ///     Gets the unique name of the measure.
        /// </summary>
        /// <value>The unique name of the measure.</value>
        public string UniqueName { get; private set; }

        /// <summary>
        ///     Gets the caption of the measure.
        /// </summary>
        /// <value>The caption.</value>
        public string Caption { get; private set; }

        /// <summary>
        ///     Gets the description of the measure.
        /// </summary>
        /// <value>The description.</value>
        public string Description { get; private set; }

        /// <summary>
        ///     Gets the name of the measure group measure belongs to.
        /// </summary>
        /// <value>The name of the measure group.</value>
        public string GroupName { get; private set; }

        /// <summary>
        ///     Gets or sets the <see cref="AggregatorType" /> that identifies how a measure was derived.
        /// </summary>
        public AggregatorType AggregatorType { get; set; }

        #endregion // Properties

        /// <summary>
        ///     Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        ///     A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            return _hashCode;
        }
    }
}