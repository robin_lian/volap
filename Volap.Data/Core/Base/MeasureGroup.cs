using Volap.Data.Core.Interfaces;

namespace Volap.Data.Core.Base
{
    /// <summary>
    ///     Represents a measure group within a cube.
    /// </summary>
    public class MeasureGroup : IMeasureGroup
    {
#pragma warning disable 436
        private static readonly IMeasureGroup _allMeasureGroup = new MeasureGroup("AllMeasuresGroupName", "AllMeasuresGroupName", string.Empty);
        // SR.GetString("AllMeasuresGroupName")
#pragma warning restore 436

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="MeasureGroup" /> class.
        /// </summary>
        /// <param name="name">The name of the measure group.</param>
        /// <param name="caption">The caption of the measure group.</param>
        /// <param name="description">The description of the measure group.</param>
        public MeasureGroup(string name, string caption, string description)
        {
            Name = name;
            Caption = caption;
            Description = description;
        }

        #endregion // Constructor

        #region Properties

        #region Implementation of IMeasureGroup		

        /// <summary>
        ///     Gets the description of the measure group.
        /// </summary>
        /// <value>The description.</value>
        public string Description { get; private set; }

        #endregion // Implementation of IMeasureGroup

        /// <summary>
        ///     Gets the instance of <see cref="MeasureGroup" /> that represents the group that holds all of the dimensions.
        /// </summary>
        public static IMeasureGroup AllMeasureGroup
        {
            get { return _allMeasureGroup; }
        }

        #endregion // Properties

        #region IOlapElement Members

        /// <summary>
        ///     Gets the name of the measure group.
        /// </summary>
        /// <value>The name of the measure group.</value>
        public string Name { get; private set; }

        /// <summary>
        ///     Gets the caption of the measure group.
        /// </summary>
        /// <value>The caption.</value>
        public string Caption { get; private set; }

        /// <summary>
        ///     GEts the unique name of this <see cref="MeasureGroup" />.
        /// </summary>
        public string UniqueName
        {
            get { return Name; }
        }

        #endregion
    }
}