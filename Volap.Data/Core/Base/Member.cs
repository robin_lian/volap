using System.Collections.Generic;
using Volap.Data.Core.Interfaces;

namespace Volap.Data.Core.Base
{
    /// <summary>
    /// Represents a member within a level
    /// </summary>
    public class Member : IMember //, IHierarchicalPositionItem<string, object>
    {
        #region Members

        private static readonly Member empty =
            new Member(
                string.Empty,
                string.Empty,
                string.Empty,
                string.Empty,
                string.Empty,
                0,
                null,
                0,
                false,
                false,
                false,
                -1);

        private readonly int _hashCode;
        private readonly List<IMember> _members = new List<IMember>();
        private object _orderByKey;
        private bool _orderByKeyAssigned;

        #endregion // Members

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="Member" /> class.
        /// </summary>
        /// <remarks> Use it only for serialization. </remarks>
        public Member()
        {
        }

        public Member(string name, string uniqueName)
        {
            Name = name;
            UniqueName = uniqueName;
            Caption = name;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Member" /> class.
        /// </summary>
        /// <param name="name">The name of the member.</param>
        /// <param name="uniqueName">The unique name of the member.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="parentUniqueName">The unique name of the parent member.</param>
        /// <param name="levelName">The unique name of the parent level.</param>
        /// <param name="levelDepth">The level depth.</param>
        /// <param name="parentLevel">The parent level.</param>
        /// <param name="childCount">The child count.</param>
        /// <param name="drilledDown">Sets whether this member is drilled down.</param>
        /// <param name="parentSameAsPrevious">
        ///     Sets whether the parent of this member is the same as the parent of the immediately
        ///     preceding member.
        /// </param>
        /// <param name="isTotal">Sets whether this member is considered to be total.</param>
        public Member(string name, string uniqueName, string caption, string parentUniqueName, string levelName, int levelDepth, ILevel parentLevel, int childCount, bool drilledDown, bool parentSameAsPrevious,
            bool isTotal) :
                this(name, uniqueName, caption, parentUniqueName, levelName, levelDepth, parentLevel, childCount, drilledDown, parentSameAsPrevious, isTotal, ++IdCounter)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Member" /> class.
        /// </summary>
        /// <param name="name">The name of the member.</param>
        /// <param name="uniqueName">The unique name of the member.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="parentUniqueName">The unique name of the parent member.</param>
        /// <param name="levelName">The unique name of the parent level.</param>
        /// <param name="levelDepth">The level depth.</param>
        /// <param name="parentLevel">The parent level.</param>
        /// <param name="childCount">The child count.</param>
        /// <param name="drilledDown">Sets whether this member is drilled down.</param>
        /// <param name="parentSameAsPrevious">
        ///     Sets whether the parent of this member is the same as the parent of the immediately
        ///     preceding member.
        /// </param>
        /// <param name="isTotal">Sets whether this member is considered to be total.</param>
        /// <param name="uniqueId">The unique id applied to this member.</param>
        protected internal Member(string name, string uniqueName, string caption, string parentUniqueName, string levelName, int levelDepth, ILevel parentLevel, int childCount, bool drilledDown,
            bool parentSameAsPrevious, bool isTotal, int uniqueId)
        {
            Name = name;
            UniqueName = uniqueName;
            Caption = caption;
            ParentUniqueName = parentUniqueName;
            LevelName = levelName;
            LevelDepth = levelDepth;
            ParentLevel = parentLevel;
            ChildCount = childCount;

            Members = new OlapElementCollection<IMember>(_members);

            DrilledDown = drilledDown;
            ParentSameAsPrevious = parentSameAsPrevious;
            IsTotal = isTotal;
            IsLoaded = false;

            UniqueId = uniqueId;
            _hashCode = uniqueName.GetHashCode();
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the <see cref="IMember.UniqueId" /> assigned to the last created member.
        /// </summary>
        protected static int IdCounter { get; set; }

        /// <summary>
        ///     Gets a value indicating whether this instance is calculated.
        /// </summary>
        public bool IsCalculated
        {
            get { return Scope.HasValue; }
        }

        /// <summary>
        ///     Gets the scope of this member. This is <c>null</c> for non-calculated members.
        /// </summary>
        /// <value>
        ///     The scope.
        /// </value>
        public int? Scope { get; set; }

        #region Empty

        /// <summary>
        ///     Gets instance of empty <see cref="Member" /> object.
        /// </summary>
        public static Member Empty
        {
            get { return empty; }
        }

        #endregion // Empty

        #region Tag

        /// <summary>
        ///     Gets or sets the user data attached to this instance.
        /// </summary>
        public object Tag { get; set; }

        #endregion // Tag

        #region OrderByKey

        /// <summary>
        ///     Gets or sets the object used as key when is performed headers sorting.
        /// </summary>
        public virtual object OrderByKey
        {
            get
            {
                if (_orderByKey == null && !_orderByKeyAssigned)
                {
                    return UniqueId;
                }

                return _orderByKey;
            }

            set
            {
                _orderByKey = value;
                _orderByKeyAssigned = true;
            }
        }

        #endregion // OrderByKey

        #region Implementation of IMember

        /// <summary>
        ///     Gets the parent member of this instance.
        /// </summary>
        public IMember ParentMember { get; internal set; }

        /// <summary>
        ///     Gets the <see cref="UniqueId" /> of the parent member.
        /// </summary>
        public int ParentUniqueId
        {
            get
            {
                if (ParentMember != null)
                {
                    return ParentMember.UniqueId;
                }

                return -1;
            }
        }

        /// <summary>
        ///     Gets the number that identifies this <see cref="IMember" /> uniquely.
        /// </summary>
        public int UniqueId { get; internal set; }

        /// <summary>
        ///     Gets the name of the member.
        /// </summary>
        /// <value>The name of the member.</value>
        public string Name { get; internal set; }

        /// <summary>
        ///     Gets the unique name of the member.
        /// </summary>
        /// <value>The unique name of the member.</value>
        public string UniqueName { get; internal set; }

        /// <summary>
        ///     Gets the caption of the member.
        /// </summary>
        /// <value>The caption.</value>
        public string Caption { get; internal set; }

        /// <summary>
        ///     Gets the parent unique name of the member.
        /// </summary>
        /// <value>The parent unique name of the member.</value>
        public string ParentUniqueName { get; internal set; }

        /// <summary>
        ///     Gets the name of the level to which this member belongs.
        /// </summary>
        /// <value>The name of the level.</value>
        public string LevelName { get; internal set; }

        /// <summary>
        ///     Gets the depth of the levelto which this member belongs.
        /// </summary>
        /// <value>The level depth.</value>
        public int LevelDepth { get; internal set; }

        /// <summary>
        ///     Gets the level that contains the Member.
        /// </summary>
        /// <value>The parent level.</value>
        public ILevel ParentLevel { get; internal set; }

        /// <summary>
        ///     Gets the calculated child count. You CAN NOT rely on this property because this can be an estimate.
        ///     Providers should return exact number if possible. The number of members in <see cref="IMember.Members" />
        ///     collection can be used instead.
        /// </summary>
        /// <value>The child count.</value>
        /// <seealso cref="IsLoaded" />
        public int ChildCount { get; internal set; }

        /// <summary>
        ///     Gets a ReadOnlyCollection with the members that are children of the current member.
        /// </summary>
        /// <value>The members.</value>
        public OlapElementCollection<IMember> Members { get; internal set; }

        #endregion // Implementation of IMember

        #region HasChildren

        /// <summary>
        ///     Determines whether the specified member has children.
        /// </summary>
        /// <returns>
        ///     <c>true</c> if the specified member has children; otherwise, <c>false</c>.
        /// </returns>
        public bool HasChildren
        {
            get
            {
                if (ParentLevel != null)
                {
                    IHierarchy hierarchy = ParentLevel.ParentHierarchy;
                    if (LevelDepth + 1 < hierarchy.Levels.Count)
                    {
                        IMember originalMember;
                        if (ParentLevel.TryGetMember(UniqueName, out originalMember))
                        {
                            return (!((Member) originalMember).IsLoaded || originalMember.Members.Count > 0);
                        }
                    }
                }

                return false;
            }
        }

        #endregion

        #region Internal

        internal List<IMember> MembersCollection
        {
            get { return _members; }
        }

        /// <summary>
        ///     Gets a value indicating whether the children members are loaded in <see cref="Members" /> collection.
        /// </summary>
        /// <value>
        ///     <c>true</c> if the children members are loaded; otherwise, <c>false</c>.
        /// </value>
        public bool IsLoaded { get; protected internal set; }

        /// <summary>
        ///     Gets a value indicating whether at least one child of this member is placed imediately after this member in result
        ///     tuple set..
        /// </summary>
        public bool DrilledDown { get; internal set; }

        /// <summary>
        ///     Gets a value indicating whether the parent of this member is the same as the parent of the immediately preceding
        ///     member.
        /// </summary>
        public bool ParentSameAsPrevious { get; internal set; }

        /// <summary>
        ///     Gets a value indicating whether this member is considered to be total.
        /// </summary>
        public bool IsTotal { get; internal set; }

        #endregion // Internal

        #endregion // Properties

        #region Methods

        private readonly IDictionary<string, IMember> _membersHash = new Dictionary<string, IMember>();

        /// <summary>
        ///     Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        ///     A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            return _hashCode;
        }

        /// <summary>
        ///     Clones this instance.
        /// </summary>
        /// <returns>Deep copy of instance.</returns>
        public virtual Member Clone(bool cloneChildren = true)
        {
            var newMember = new Member(
                Name,
                UniqueName,
                Caption,
                ParentUniqueName,
                LevelName,
                LevelDepth,
                ParentLevel,
                ChildCount,
                DrilledDown,
                ParentSameAsPrevious,
                IsTotal,
                UniqueId);

            newMember.Tag = Tag;
            newMember.OrderByKey = OrderByKey;
            newMember.ParentMember = ParentMember;
            newMember.Scope = Scope;
            newMember.IsLoaded = IsLoaded;

            if (cloneChildren)
            {
                foreach (IMember member in MembersCollection)
                {
                    newMember.MembersCollection.Add(((Member) member).Clone());
                }
            }

            //if (!object.ReferenceEquals(this.UniqueName, newMember.UniqueName))
            //{
            //    throw new Exception();
            //}

            return newMember;
        }

        internal void AddMember(IMember member)
        {
            lock (MembersCollection)
            {
                if (_membersHash.ContainsKey(member.UniqueName))
                {
                    return;
                }

                _membersHash.Add(member.UniqueName, member);
                MembersCollection.Add(member);

                ChildCount = MembersCollection.Count;
                ((Member) member).ParentMember = this;
            }
        }

        internal void RemoveMember(IMember member)
        {
            lock (MembersCollection)
            {
                if (!_membersHash.ContainsKey(member.UniqueName))
                {
                    return;
                }

                _membersHash.Remove(member.UniqueName);
                MembersCollection.Remove(member);

                ChildCount = MembersCollection.Count;
                ((Member) member).ParentMember = null;
            }
        }

        #region Override methods

        /// <summary>
        ///     Returns a <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </returns>
        public override string ToString()
        {
            return Caption;
        }

        #endregion

        #endregion // Methods

        /// <summary>
        ///     Gets the tuple this member belongs to.
        /// </summary>
        public ITuple Tuple { get; internal set; }

        /*IPosition<string, object> IPositionItem<string, object>.Position
        {
            get { return this.Tuple as IPosition<string, object>; }
        }

        string IPositionItem<string, object>.Key
        {
            get { return this.UniqueName; }
        }

        object IPositionItem<string, object>.SortKey
        {
            get { return this.OrderByKey; }
        }

        int IPositionItem<string, object>.Ordinal
        {
            get { return this.UniqueId; }
        }

        string IHierarchicalPositionItem<string, object>.ParentKey
        {
            get { return this.ParentUniqueName; }
        }

        string IHierarchicalPositionItem<string, object>.HierarchyKey
        {
            get 
            {
                if (this.ParentLevel == null || this.ParentLevel.ParentHierarchy == null)
                {
                    return null;
                }
                else
                {
                    return this.ParentLevel.ParentHierarchy.UniqueName;
                }
            }
        }

        string IHierarchicalPositionItem<string, object>.HierarchyLevelKey
        {
            get { return this.LevelName; }
        }

        int IHierarchicalPositionItem<string, object>.Depth
        {
            get { return this.LevelDepth; }
        }

        bool IPositionItem<string, object>.IsExpandable
        {
            get { return this.ChildCount > 0; }
        }*/
    }
}