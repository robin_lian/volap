﻿using System.Collections.Generic;
using Volap.Data.Core.Interfaces;

namespace Volap.Data.Core.Base
{
    /// <summary>
    ///     Represents the schema for the olap data.
    /// </summary>
    public class OlapSchema : ISchema
    {
        #region Members

        private readonly List<ICube> _cubes = new List<ICube>();

        #endregion // Members

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="OlapSchema" /> class.
        /// </summary>
        public OlapSchema()
        {
            Cubes = new OlapElementCollection<ICube>(_cubes);
        }

        #endregion // Constructor

        #region Properties

        #region Implementation of ISchema

        /// <summary>
        ///     Gets a ReadOnlyCollection with the enumeration of cubes included in the schema.
        /// </summary>
        /// <value>The cubes.</value>
        public OlapElementCollection<ICube> Cubes { get; private set; }

        #endregion // Implementation of ISchema

        #region Internal

        internal List<ICube> CubesCollection
        {
            get { return _cubes; }
        }

        #endregion // Internal

        #endregion Properties
    }
}