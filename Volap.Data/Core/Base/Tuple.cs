﻿using System.Collections.Generic;
using Volap.Data.Core.Interfaces;

namespace Volap.Data.Core.Base
{
    /// <summary>
    ///     Represents an ordered sequence of members.
    /// </summary>
    public class Tuple : ITuple //, IPosition<string, object>
    {
        #region Members

        private static readonly Tuple emptyTuple = new Tuple(new OlapElementCollection<IMember>(new List<IMember> {Member.Empty}));

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="Tuple" /> class.
        /// </summary>
        /// <param name="members">The members included in the tuple.</param>
        public Tuple(OlapElementCollection<IMember> members)
        {
            Members = members;

            //foreach (IMember member in members)
            //{
            //    //((Member) member).Tuple = this;
            //}
        }

        #endregion

        #region Properties

        #region Empty

        /// <summary>
        ///     Gets instance of empty <see cref="Tuple" /> object.
        /// </summary>
        public static Tuple Empty
        {
            get { return emptyTuple; }
        }

        #endregion // Empty

        #region Implementation of ITuple

        /// <summary>
        ///     Gets the members included in the tuple.
        /// </summary>
        /// <value>The members included in the tuple.</value>
        public OlapElementCollection<IMember> Members { get; private set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance is total.
        /// </summary>
        /// <value><c>true</c> if this instance is total; otherwise, <c>false</c>.</value>
        public bool IsTotal { get; set; }

        #endregion // Implementation of ITuple

        #endregion // Properties

        /// <summary>
        ///     Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        ///     A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            string result = string.Empty;
            foreach (IMember member in Members)
            {
                result += member.Caption + ":";
            }

            return result.Remove(result.Length - 1);
        }

        /*IEnumerable<IPositionItem<string, object>> IPosition<string, object>.GetItems()
        {
            IEnumerator<IMember> enumerator = this.Members.GetEnumerator();
            while (enumerator.MoveNext())
            {
                yield return enumerator.Current as IPositionItem<string, object>;
            }
        }

        int IPosition<string, object>.ItemsCount
        {
            get { return this.Members.Count; }
        }

        int IPosition<string, object>.IndexOf(IPositionItem<string, object> tupleItem)
        {
            IMember member = tupleItem as IMember;
            if (member == null)
            {
                return -1;
            }

            return this.Members.IndexOf(member);
        }

        IPositionItem<string, object> IPosition<string, object>.this[int itemIndex]
        {
            get { return this.Members[itemIndex] as IPositionItem<string, object>; }
        }

        bool IPosition<string, object>.IsEmpty
        {
            get { return this == Base.Tuple.Empty; }
        }*/
    }
}