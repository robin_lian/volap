﻿using System;
using System.ComponentModel;
using Volap.Data.Core.Base;
using Volap.Data.Core.Interfaces;

namespace Volap.Data.Core
{
	/// <summary>
	/// Specifies the location for measure list dimention element.
	/// </summary>
    public enum MeasureListLocation
    {
		/// <summary>
        /// Specifies that measure list element is placed in the columns collection.
		/// </summary>
        Columns,

		/// <summary>
        /// Specifies that measure list element is placed in the rows collection.
		/// </summary>
		Rows
    }

    /// <summary>
    /// Specifies the expansion behavior of the header nodes when in given area are added multiple hierarchies.
    /// This beahvior is applied in the conjunction with the expansion state of <see cref="IFilterViewModel.FilterMembers"/>
    /// related to given <see cref="IHierarchy"/>.
    /// </summary>
    public enum HierarchyExpansionMode
    {
        /// <summary>
        /// Specifies that only the nodes related the top most <see cref="IHierarchy"/> should be syncronized with the expansion state 
        /// of <see cref="IFilterViewModel.FilterMembers"/> created over that hierarchy.
        /// </summary>
        [Obsolete("Use HierarchyExpansionMode.ExpandTopMostOnly instead.")]
        [EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
        SingleLevelOnly,

        /// <summary>
        /// Specifies that only the nodes related the top most <see cref="IHierarchy"/> should be syncronized with the expansion state 
        /// of <see cref="IFilterViewModel.FilterMembers"/> created over that hierarchy.
        /// </summary>
        ExpandTopMostOnly,

        /// <summary>
        /// Specifies that the nodes of all hierarchies should be syncronized with the expansion state 
        /// of <see cref="IFilterViewModel.FilterMembers"/> created over that hierarchy.
        /// </summary>
        [Obsolete("Use HierarchyExpansionMode.ExpandAll instead.")]
        [EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
        DrillDown,

        /// <summary>
        /// Specifies that the nodes of all hierarchies should be syncronized with the expansion state 
        /// of <see cref="IFilterViewModel.FilterMembers"/> created over that hierarchy.
        /// </summary>
        ExpandAll
    }

    #region HierarchyOrigin

    /// <summary>
    ///  An enumeration of different hierarchy origin.
    /// </summary>
    [Flags]
	public enum HierarchyOrigin
	{
        /// <summary>
        /// Identifies user defined hierarchies.
        /// </summary>
		UserDefined = 1,

        /// <summary>
        /// Identifies attribute hierarchies.
        /// </summary>
		SystemEnabled = 2,

        /// <summary>
        /// Identifies attributes with no attribute .
        /// </summary>
		SystemInternal = 4
    }

    #endregion // HierarchyOrigin

    #region ItemTypes

    /// <summary>
    /// Enumeration of different type of meta items that <see cref="HierarchicalItem"/> can represents.
    /// </summary>
    public enum ItemTypes
    {
        /// <summary>
        /// Displays the <see cref="ICube"/> items.
        /// </summary>
        Cube,

        /// <summary>
        /// Displays dimmension of the cube.
        /// </summary>
        Dimension,	
	
        /// <summary>
        /// Displays group of items.
        /// </summary>
		Group,

        /// <summary>
        /// Displays <see cref="HierarchyOrigin.UserDefined"/> items.
        /// </summary>
		UserDefinedHierarchy,

        /// <summary>
        /// Displays <see cref="HierarchyOrigin.SystemEnabled"/> items.
        /// </summary>
		SystemEnabledHierarchy,

        /// <summary>
        /// Displays <see cref="HierarchyOrigin.SystemInternal"/> items.
        /// </summary>
		ParentChildHierarchy,

        /// <summary>
        /// Displays <see cref="IMeasure"/> items.
        /// </summary>
        Measure,

        /// <summary>
        /// Displays <see cref="ILevel"/> items on specified level.
        /// </summary>
		Level1,

        /// <summary>
        /// Displays <see cref="ILevel"/> items on specified level.
        /// </summary>
		Level2,

        /// <summary>
        /// Displays <see cref="ILevel"/> items on specified level.
        /// </summary>
		Level3,

        /// <summary>
        /// Displays <see cref="ILevel"/> items on specified level.
        /// </summary>
		Level4,

        /// <summary>
        /// Displays <see cref="ILevel"/> items on specified level.
        /// </summary>
		Level5
    }

    #endregion //ItemTypes

    #region AggregatorType

    /// <summary>
    ///  An enumeration of different function that can be applied to measures.
    /// </summary>    
    public enum AggregatorType
    {
        /// <summary>
        /// The aggregated function is undefined.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// The aggregated function adds all values.
        /// </summary>
        Sum = 1,

        /// <summary>
        /// The aggregated function will count the number of the values.
        /// </summary>
        Count = 2,

        /// <summary>
        /// The aggregated function will returns the smallest value.
        /// </summary>
        Min = 3,
      
        /// <summary>
        /// The aggregated function will returns the largest value.
        /// </summary>
        Max = 4,

        /// <summary>
        /// The aggregated function will returns the average of cells value.
        /// </summary>
        Average = 5,

        /// <summary>
        /// The aggregated function will estimates variance based on the sample.
        /// </summary>
        Var = 6,

        /// <summary>
        /// The aggregated function will estimates the standart deviation based on sample.
        /// </summary>
        Std = 7,

        /// <summary>
        /// The aggregated function will returns the number of distinct, nonempty tuples in a set.
        /// </summary>
        DistinctCount = 8,

        /// <summary>
        /// No aggregation performed.
        /// </summary>
        None = 9,

        /// <summary>
        /// The aggregated function will returns the average of the measure's children.
        /// </summary>
        AverageOfChildren = 10,

        /// <summary>
        /// The aggregated function will returns the measure's first nonempty member.
        /// </summary>
        FirstNonEmpty = 13,

        /// <summary>
        /// The aggregated function will returns  the measure's last nonempty member.
        /// </summary>
        LastNonEmpty = 14,

        /// <summary>
        /// Aggregated by the aggregation function associated with the specified account type of an attribute in an account dimension.
        /// </summary>
        ByAccount = 15,

        /// <summary>
        /// The aggregated function will returns the result derived from a formula.
        /// </summary>
        Calculated = 127        
    }

    #endregion // AggregatorType

    public enum ScopeType
    {
        /// <summary>
        /// It cannot be determined the scope of the <see cref="KeyPerformanceIndicator"/> .
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// The scope of the <see cref="KeyPerformanceIndicator"/> is global.
        /// </summary>
        Global = 1,

        /// <summary>
        /// The scope of the <see cref="KeyPerformanceIndicator"/> is session.
        /// </summary>
        Session = 2
    }

/*    #region ExecutionContext

    /// <summary>
    /// Specifies the context in which given operation is considered.
    /// </summary>
    public enum ExecutionContext
    {
        /// <summary>
        /// The action is considered in the context of the existing rows.
        /// </summary>
        Rows,

        /// <summary>
        /// The action is considered in the context of the existing columns.
        /// </summary>
        Columns
    }

    #endregion*/
}
