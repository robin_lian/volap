﻿using System.Collections.Generic;

namespace Volap.Data.Core.Interfaces
{
    /// <summary>
    /// The ICube interface represents an OLAP cube that holds a multi dimensional representation of a flat data set.
    /// A cube is analogous to a table in a relational database. Where a table in a relational database has two dimensions, a cube can have any number of dimensions. In its simplest form, the dimensions of a cube correspond to a field of the flat data set.
    /// </summary>
    public interface ICube : IOlapElement
    {
        /// <summary>
        /// Gets the name of the catalog this cube belongs to.
        /// </summary>
        string CatalogName
        {
            get;
        }

        /// <summary>
        /// Gets the description of the cube.
        /// </summary>
        /// <value>The description.</value>
        string Description { get; }

        /// <summary>
        /// Gets an ReadOnlyCollection with the measures that are defined for the cube.
        /// </summary>
        /// <value>The measures.</value>
        OlapElementCollection<IMeasure> Measures { get; }

        /// <summary>
        ///  Gets an ReadOnlyCollection with the dimensions that are defined for the cube.
        /// </summary>
        /// <value>The dimensions.</value>
        OlapElementCollection<IDimension> Dimensions { get; }

		/// <summary>
		///  Gets an ReadOnlyCollection with the measure groups that are defined for the cube.
		/// </summary>
		/// <value>The measure groups.</value>
		OlapElementCollection<IMeasureGroup> MeasureGroups { get; }

		/// <summary>
		///  Gets a dictionary with the measure groups and list of dimensions for each measure group that are defined for the cube.
		/// </summary>
		Dictionary<IMeasureGroup, List<IDimension>> MeasureGroupDimensions { get; }

        /// <summary>
        /// Gets a measure dimension of a cube.
        /// </summary>
        IDimension MeasureDimension { get; }

    }
}
