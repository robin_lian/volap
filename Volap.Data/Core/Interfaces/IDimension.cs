﻿namespace Volap.Data.Core.Interfaces
{
    /// <summary>
    /// Represents a dimension within a cube
    /// </summary>
    public interface IDimension : IOlapElement
    {
        #region Properties

        /// <summary>
        /// Gets the description of the dimension.
        /// </summary>
        /// <value>The description.</value>
        string Description { get; }

        /// <summary>
        /// Gets a ReadOnlyCollection with the hierarchies within the dimension.
        /// </summary>
        /// <value>Hierarchies within the dimension.</value>
        OlapElementCollection<IHierarchy> Hierarchies { get; }

        #endregion Properties
    }
}
