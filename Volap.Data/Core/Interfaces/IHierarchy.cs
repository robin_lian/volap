﻿
namespace Volap.Data.Core.Interfaces
{
    /// <summary>
    /// Represents a hierarchy within a dimension
    /// </summary>
    public interface IHierarchy : IOlapElement
    {

        /// <summary>
        /// Gets the description of the hierarchy.
        /// </summary>
        /// <value>The description.</value>
        string Description { get; }

        /// <summary>
        /// Gets the unique name of the default member for the Hierarchy
        /// </summary>
        /// <value>The unique name of the default member for the Hierarchy.</value>
        string DefaultMember { get; }

        /// <summary>
        /// Gets the "All" member of this hierarchy.
        /// </summary>
        /// <value>The "All" member.</value>
        string AllMember { get; }

        /// <summary>
        /// Gets the dimension that contains the hierarchy.
        /// </summary>
        /// <value>The parent dimension.</value>
        IDimension ParentDimension { get; }

        /// <summary>
        /// Gets a ReadOnlyCollection with the levels within the hierarchy.
        /// </summary>
        /// <value>Levels within the hierarchy.</value>
        OlapElementCollection<ILevel> Levels { get; }

		/// <summary>
		/// Gets the name of the measue group hierarachy belongs to.
		/// </summary>
		string GroupName { get; }

		/// <summary>
		/// Gets the source of the hierarchy.
		/// </summary>
		HierarchyOrigin HierarchyOrigin { get; }

    }
}
