﻿namespace Volap.Data.Core.Interfaces
{
    /// <summary>
    /// Represents a level within a hierarchy
    /// </summary>
    public interface ILevel : IOlapElement
    {

        /// <summary>
        /// Gets the description of the level.
        /// </summary>
        /// <value>The description.</value>
        string Description { get; }

        /// <summary>
        /// Gets the distance of the level from the root of the level. Root level is zero (0)
        /// </summary>
        /// <value>The depth.</value>
        int Depth { get; }

        /// <summary>
        /// Gets the count of all members in the level.
        /// </summary>
        /// <value>The members count.</value>
        int MembersCount { get; }

        /// <summary>
        /// Gets the hierarchy that contains the level.
        /// </summary>
        /// <value>The parent hierarchy.</value>
        IHierarchy ParentHierarchy { get; }

        /// <summary>
        /// Gets a collection with the members currently loaded within the level.
        /// </summary>
        /// <value>Members within the level.</value>
        OlapElementCollection<IMember> Members { get; }

        /// <summary>
        /// Gets the member associated with the specified name.
        /// </summary>
        /// <param name="memberName">The member unique name.</param>
        /// <param name="member">The member.</param>
        /// <returns></returns>
        bool TryGetMember(string memberName, out IMember member);

    }
}
