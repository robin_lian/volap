﻿namespace Volap.Data.Core.Interfaces
{
    /// <summary>
    /// Represents a measure within a cube
    /// </summary>
    public interface IMeasure : IOlapElement
    {

        /// <summary>
        /// Gets the description of the measure.
        /// </summary>
        /// <value>The description.</value>
        string Description { get; }

		/// <summary>
		/// Gets the name of the measue group measure belongs to.
		/// </summary>
		string GroupName { get; }

        /// <summary>
        /// Gets or sets the <see cref="AggregatorType"/> that identifies how a measure was derived.
        /// </summary>
        AggregatorType AggregatorType { get; set; }

    }
}
