﻿namespace Volap.Data.Core.Interfaces
{
    /// <summary>
    /// Represents a dimension within a cube
    /// </summary>
    public interface IMeasureGroup : IOlapElement
    {
        /// <summary>
        /// Gets the description of the measure group.
        /// </summary>
		/// <value>The description of measure group.</value>
        string Description { get; }

    }
}
