﻿namespace Volap.Data.Core.Interfaces
{
    /// <summary>
    /// Represents a member within a level
    /// </summary>
    public interface IMember : IOlapElement
    {
        /// <summary>
        /// Gets the parent member.
        /// </summary>
        /// <value>The parent member.</value>
        IMember ParentMember { get; }

        /// <summary>
        /// Gets the parent unique name of the member.
        /// </summary>
        /// <value>The parent unique name of the member.</value>
        string ParentUniqueName { get; }

        /// <summary>
        /// Gets the <see cref="UniqueId"/> of the parent member.
        /// </summary>
        int ParentUniqueId { get; }

        /// <summary>
        /// Gets the name of the level to which this member belongs.
        /// </summary>
        /// <value>The name of the level.</value>
        string LevelName { get; }

        /// <summary>
        /// Gets the depth of the level to which this member belongs.
        /// </summary>
        /// <value>The level depth.</value>
        int LevelDepth { get; }

        /// <summary>
        /// Gets the level that contains the Member.
        /// </summary>
        /// <value>The parent level.</value>
        ILevel ParentLevel { get; }

        /// <summary>
        /// Gets the calculated child count. You CAN NOT rely on this property because this can be an estimate. 
        /// Providers should return exact number if possible. The number of members in <see cref="IMember.Members"/> collection can be used instead.
        /// </summary>
        /// <value>The child count.</value>
        int ChildCount { get; }

        /// <summary>
        /// Gets a value indicating whether at least one child of this member is placed imediately after this member in result tuple set.. 
        /// </summary>
        bool DrilledDown { get; }

        /// <summary>
        /// Gets a value indicating whether this member has the same parent as the member at the same position 
        /// from the previous tuple in the result set.
        /// </summary>
        bool ParentSameAsPrevious { get; }

        /// <summary>
        /// Gets a value indicating whether this member is total.
        /// </summary>
        bool IsTotal { get; }

        /// <summary>
        /// Gets the object used as key when is performed headers sorting.
        /// </summary>
        object OrderByKey { get; }

        /// <summary>
        /// Gets a ReadOnlyCollection with the members that are childs of the current member.
        /// </summary>
        /// <value>The members.</value>
        OlapElementCollection<IMember> Members { get; }

        /// <summary>
        /// Gets the number that identifies this <see cref="IMember"/> uniquely.
        /// </summary>
        int UniqueId { get; }

        /// <summary>
        /// Gets the user data attached to this instance.
        /// </summary>
        object Tag { get; }

        /// <summary>
        /// Gets a value indicating whether this instance is calculated.
        /// </summary>
        bool IsCalculated { get; }

        /// <summary>
        /// Gets the tuple this member belongs to.
        /// </summary>
        ITuple Tuple
        {
            get;
        }

        /// <summary>
        /// Gets the scope of this member. This is <c>null</c> for non-calculated members.
        /// </summary>
        /// <value>The scope.</value>
        int? Scope
        {
            get;
        }

        /// <summary>
        /// Determines whether the specified member has children.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if the specified member has children; otherwise, <c>false</c>.
        /// </returns>
        bool HasChildren { get; }
    
    }
}
