﻿namespace Volap.Data.Core.Interfaces
{
    /// <summary>
    /// Interface used for define common properties of OLAP elements
    /// </summary>
    public interface IOlapElement
    {
        /// <summary>
        /// Gets the name of the element.
        /// </summary>
        /// <value>The name of the element.</value>
        string Name { get; }

        /// <summary>
        /// Gets the unique name of the element.
        /// </summary>
        /// <value>The unique name of the element.</value>
        string UniqueName { get; }

        /// <summary>
        /// Gets the caption of the element.
        /// </summary>
        /// <value>The caption of the element.</value>
        string Caption { get; }
    }
}
