﻿namespace Volap.Data.Core.Interfaces
{
    /// <summary>
    /// Represents the schema for the olap data. 
    /// </summary>
    public interface ISchema
    {
        /// <summary>
        /// Gets a ReadOnlyCollection with the enumeration of cubes included in the schema.
        /// </summary>
        /// <value>The cubes.</value>
        OlapElementCollection<ICube> Cubes { get; }
    }
}
