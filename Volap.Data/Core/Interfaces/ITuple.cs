﻿namespace Volap.Data.Core.Interfaces
{
    /// <summary>
    /// Represents an ordered sequence of members.
    /// </summary>
    public interface ITuple
    {
        /// <summary>
        /// Gets the members included in the tuple.
        /// </summary>
        /// <value>The members included in the tuple.</value>
        OlapElementCollection<IMember> Members { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is total.
        /// </summary>
        /// <value><c>true</c> if this instance is total; otherwise, <c>false</c>.</value>
        bool IsTotal { get; set; }
    }
}
