﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Volap.Data.Core.Interfaces;

namespace Volap.Data.Core
{
    /// <summary>
    /// Contains a indexed collection of OLAP elements
    /// </summary>
    /// <typeparam name="T">Generic parameter should be class of type IOlapElement</typeparam>
    public class OlapElementCollection<T> : Collection<T> where T : class, IOlapElement // ReadOnly
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OlapElementCollection&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="list">The collection to wrap.</param>
        public OlapElementCollection(IList<T> list) : base(list)
        {
        }

        /// <summary>
        /// Gets or sets a value indicating whether the requested item should be matched only by its <see cref="IOlapElement.UniqueName"/> when an item of this collection is accessed by its string index,
        /// otherwise if item with such unique name is not found the specified index value is verified against <see cref="IOlapElement.Name"/> too.        
        /// </summary>
        /// <value>
        /// 	<c>true</c> if only <see cref="IOlapElement.UniqueName"/> match is used; otherwise, <c>false</c>.
        /// </value>
        public bool UniqueNameIndexesOnly
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the element with the specified name.
        /// </summary>
        /// <param name="name">The element with specified name</param>
        public T this[string name]
        {
            get
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    IOlapElement olapElement = Items[i];
                    if (olapElement != null && olapElement.UniqueName == name)
                    {
                        return olapElement as T;
                    }
                }

                if (UniqueNameIndexesOnly)
                {
                    return null;
                }

                for (int i = 0; i < Items.Count; i++)
                {
                    IOlapElement olapElement = Items[i];
                    if (olapElement != null && olapElement.Name == name)
                    {
                        return olapElement as T;
                    }
                }

                return null;
            }
        }
    }
}
