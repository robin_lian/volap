﻿using System.Collections.Generic;
using Volap.Data.Core.Base;
using Volap.Data.Models;

namespace Volap.Data.Core
{
    public class VolapReport
    {

        public string Name { get; set; }

        public string CubeName { get; set; }

        private List<Base.Measure> measures = new List<Base.Measure>();

        private List<Field> categoryDims = new List<Field>();

        private List<Field> slicerDims = new List<Field>();


        public VolapReport(string name, string cubeName)
        {
            Name = name;
            CubeName = cubeName;
        }

        // create category dim with single level
        public void AddDimension(int type, string name, string levelun)
        {
            string[] levelArr = levelun.Split('.');

            Level level = new Level(name, levelun);
            //Hierarchy hier = new Hierarchy(name, levelArr[0] + "." + levelArr[1]);
            //hier.Levels.Add(level);

            Field dim = new Field
            {
                Name = name,
                //UniqueName = name
            };

            //dim.MeasureGroup_Dimension_Mappings
            //dim.Hierarchies.Add(hier);

            switch (type)
            {
                /*case 0:
                    measureDims.Add(dim);
                    break;*/
                case 1:
                    categoryDims.Add(dim);
                    break;
                case 2:
                    slicerDims.Add(dim);
                    break;

            }
        }

        
        public void AddMeasure(string name, string uniqueName)
        {
            measures.Add(new Base.Measure(name, uniqueName));
        }

        public void AddMember(Level level, string name, string uniqueName)
        {
            level.Members.Add(new Member(name, uniqueName));
        }

        public Level GetSlicerLevel(string uniqueName)
        {
            foreach (Field dim in slicerDims)
            {
                /*foreach (Hierarchy hier in dim.Hierarchies)
                {
                    foreach (Level level in hier.Levels)
                    {
                        if (level.UniqueName.Equals(uniqueName))
                            return level;
                    }
                }*/
            }
            return null;
        }
    }
}
