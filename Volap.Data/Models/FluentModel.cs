using System.Configuration;
using System.Linq;
using Telerik.OpenAccess;
using Telerik.OpenAccess.Metadata;

namespace Volap.Data.Models	
{
    public partial class FluentModel : OpenAccessContext
    {

        private static string connectionStringName = "VolapConnection";

        private static MetadataContainer metadataContainer = new FluentModelMetadataSource().GetModel();

        private static BackendConfiguration backendConfiguration = new BackendConfiguration
        {
            Backend = "mssql",
            ProviderName = "System.Data.SqlClient"
        };

        private IObjectScope objectScope;

        public FluentModel() : base(connectionStringName, backendConfiguration, metadataContainer)
        {

        }

        //custom property allowing to check whether the context instance is using the online database
        //public ContextMode Mode { get; }

        //only used to set the CommandTimeout for the purpose of ContextSwitchTests
        public BackendConfiguration BackendConfiguration
        {
            get
            {
                if (objectScope == null)
                {
                    objectScope = GetScope();
                }
                return objectScope.Database.BackendConfiguration;
            }
        }

        public IQueryable<Connection> Connections => GetAll<Connection>();

        public IQueryable<Field> Fields => GetAll<Field>();

        public IQueryable<MeasureGroup> MeasureGroups => GetAll<MeasureGroup>();


        //creates or updates the database schema based on the current model
        public void CreateUpdateDatabase()
        {
            /*ISchemaHandler handler = GetSchemaHandler();

            string ddlScript;

            if (handler.DatabaseExists())
            {
                ddlScript = handler.CreateUpdateDDLScript(null);
            }
            else
            {
                handler.CreateDatabase();
                ddlScript = handler.CreateDDLScript();
            }

            if (string.IsNullOrEmpty(ddlScript) == false)
            {
                handler.ExecuteDDLScript(ddlScript);
            }*/
        }
		
		/// <summary>
		/// Allows you to customize the BackendConfiguration of FluentModel.
		/// </summary>
		/// <param name="config">The BackendConfiguration of FluentModel.</param>
		static partial void CustomizeBackendConfiguration(ref BackendConfiguration config);
		
	}
}
