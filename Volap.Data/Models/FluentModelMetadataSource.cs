
using System.Collections.Generic;
using Telerik.OpenAccess;
using Telerik.OpenAccess.Metadata;
using Telerik.OpenAccess.Metadata.Fluent;

namespace Volap.Data.Models
{

	public class FluentModelMetadataSource : FluentMetadataSource
	{
		
		protected override IList<MappingConfiguration> PrepareMapping()
		{
			List<MappingConfiguration> mappingConfigurations = new List<MappingConfiguration>();
			
			MappingConfiguration<MeasureGroup> measuregroupConfiguration = this.GetMeasureGroupMappingConfiguration();
			mappingConfigurations.Add(measuregroupConfiguration);
			
			MappingConfiguration<Measure> measureConfiguration = this.GetMeasureMappingConfiguration();
			mappingConfigurations.Add(measureConfiguration);
			
			MappingConfiguration<Hierarchy> hierarchyConfiguration = this.GetHierarchyMappingConfiguration();
			mappingConfigurations.Add(hierarchyConfiguration);
			
			MappingConfiguration<Field> fieldConfiguration = this.GetFieldMappingConfiguration();
			mappingConfigurations.Add(fieldConfiguration);
			
			MappingConfiguration<Cube> cubeConfiguration = this.GetCubeMappingConfiguration();
			mappingConfigurations.Add(cubeConfiguration);
			
			MappingConfiguration<Connection> connectionConfiguration = this.GetConnectionMappingConfiguration();
			mappingConfigurations.Add(connectionConfiguration);
			
			return mappingConfigurations;
		}
		
		protected override void SetContainerSettings(MetadataContainer container)
		{
			container.Name = "FluentModel";
			container.DefaultNamespace = "Volap.Data";
			container.NameGenerator.RemoveLeadingUnderscores = false;
			container.NameGenerator.SourceStrategy = Telerik.OpenAccess.Metadata.NamingSourceStrategy.Property;
			container.NameGenerator.RemoveCamelCase = false;
		}

		public MappingConfiguration<MeasureGroup> GetMeasureGroupMappingConfiguration()
		{
			MappingConfiguration<MeasureGroup> configuration = this.GetMeasureGroupClassConfiguration();
			this.PrepareMeasureGroupPropertyConfigurations(configuration);
			this.PrepareMeasureGroupAssociationConfigurations(configuration);

			return configuration;
		}

		public MappingConfiguration<MeasureGroup> GetMeasureGroupClassConfiguration()
		{
			MappingConfiguration<MeasureGroup> configuration = new MappingConfiguration<MeasureGroup>();
			configuration.MapType(x => new { }).WithConcurencyControl(OptimisticConcurrencyControlStrategy.Changed).ToTable("MeasureGroup");
	
			return configuration;
		}
	
		public void PrepareMeasureGroupPropertyConfigurations(MappingConfiguration<MeasureGroup> configuration)
		{
			configuration.HasProperty(x => x.Id).IsIdentity(KeyGenerator.Autoinc).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Id").IsNotNullable().HasColumnType("int").HasPrecision(0).HasScale(0);
			configuration.HasProperty(x => x.CubeId).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("CubeId").IsNotNullable().HasColumnType("int").HasPrecision(0).HasScale(0);
			configuration.HasProperty(x => x.Name).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Name").IsNullable().HasColumnType("varchar").HasLength(100);
			configuration.HasProperty(x => x.NamedQuery).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("NamedQuery").IsNullable().HasColumnType("varchar").HasLength(5000);
			configuration.HasProperty(x => x.DateColumn).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("DateColumn").IsNullable().HasColumnType("varchar").HasLength(50);
			configuration.HasProperty(x => x.Measures).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Measures").IsNullable().HasColumnType("varchar").HasLength(1000);
			configuration.HasProperty(x => x.MinDate).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("MinDate").IsNullable().HasColumnType("datetime");
			configuration.HasProperty(x => x.LastUpdate).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("LastUpdate").IsNullable().HasColumnType("datetime");
			configuration.HasProperty(x => x.Active).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Active").IsNullable().HasColumnType("bit").HasPrecision(0).HasScale(0);
			configuration.HasProperty(x => x.BusinessKey).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("BusinessKey").IsNullable().HasColumnType("varchar").HasLength(50);
			configuration.HasProperty(x => x.LongText).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("LongText").IsNullable().HasColumnType("varchar").HasLength(1000);
		}
	
		public void PrepareMeasureGroupAssociationConfigurations(MappingConfiguration<MeasureGroup> configuration)
		{
			configuration.HasAssociation(x => x.Cube).WithOpposite(x => x.MeasureGroups).ToColumn("CubeId").HasConstraint((x, y) =>  x.CubeId == y.Id ).IsRequired().WithDataAccessKind(DataAccessKind.ReadWrite);
			configuration.HasAssociation(x => x.MeasuresList).HasFieldName("measures").WithOpposite(x => x.MeasureGroup).ToColumn("MeasureGroupId").HasConstraint((y, x) =>  x.MeasureGroupId == y.Id ).WithDataAccessKind(DataAccessKind.ReadWrite);
			configuration.HasAssociation(x => x.Fields).WithOpposite(x => x.MeasureGroup).ToColumn("MeasureGroupId").HasConstraint((y, x) =>  x.MeasureGroupId == y.Id ).WithDataAccessKind(DataAccessKind.ReadWrite);
		}
		
		public MappingConfiguration<Measure> GetMeasureMappingConfiguration()
		{
			MappingConfiguration<Measure> configuration = this.GetMeasureClassConfiguration();
			this.PrepareMeasurePropertyConfigurations(configuration);
			this.PrepareMeasureAssociationConfigurations(configuration);

			return configuration;
		}

		public MappingConfiguration<Measure> GetMeasureClassConfiguration()
		{
			MappingConfiguration<Measure> configuration = new MappingConfiguration<Measure>();
			configuration.MapType(x => new { }).WithConcurencyControl(OptimisticConcurrencyControlStrategy.Changed).ToTable("Measure");
	
			return configuration;
		}
	
		public void PrepareMeasurePropertyConfigurations(MappingConfiguration<Measure> configuration)
		{
			configuration.HasProperty(x => x.Id).IsIdentity(KeyGenerator.Autoinc).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Id").IsNotNullable().HasColumnType("int").HasPrecision(0).HasScale(0);
			configuration.HasProperty(x => x.MeasureGroupId).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("MeasureGroupId").IsNotNullable().HasColumnType("int").HasPrecision(0).HasScale(0);
			configuration.HasProperty(x => x.Name).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Name").IsNullable().HasColumnType("varchar").HasLength(100);
			configuration.HasProperty(x => x.Caption).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Caption").IsNullable().HasColumnType("varchar").HasLength(100);
			configuration.HasProperty(x => x.UniqueName).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("UniqueName").IsNullable().HasColumnType("varchar").HasLength(100);
			configuration.HasProperty(x => x.Aggregator).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Aggregator").IsNullable().HasColumnType("smallint").HasPrecision(0).HasScale(0);
			configuration.HasProperty(x => x.Visible).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Visible").IsNullable().HasColumnType("bit").HasPrecision(0).HasScale(0);
			configuration.HasProperty(x => x.DisplayFolder).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("DisplayFolder").IsNullable().HasColumnType("varchar").HasLength(100);
			configuration.HasProperty(x => x.Format).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Format").IsNullable().HasColumnType("varchar").HasLength(100);
		}
	
		public void PrepareMeasureAssociationConfigurations(MappingConfiguration<Measure> configuration)
		{
            // .HasFieldName("measures")
            configuration.HasAssociation(x => x.MeasureGroup).WithOpposite(x => x.MeasuresList).ToColumn("MeasureGroupId").HasConstraint((x, y) =>  x.MeasureGroupId == y.Id ).IsRequired().WithDataAccessKind(DataAccessKind.ReadWrite);
		}
		
		public MappingConfiguration<Hierarchy> GetHierarchyMappingConfiguration()
		{
			MappingConfiguration<Hierarchy> configuration = this.GetHierarchyClassConfiguration();
			this.PrepareHierarchyPropertyConfigurations(configuration);
			this.PrepareHierarchyAssociationConfigurations(configuration);

			return configuration;
		}

		public MappingConfiguration<Hierarchy> GetHierarchyClassConfiguration()
		{
			MappingConfiguration<Hierarchy> configuration = new MappingConfiguration<Hierarchy>();
			configuration.MapType(x => new { }).WithConcurencyControl(OptimisticConcurrencyControlStrategy.Changed).ToTable("Hierarchy");
	
			return configuration;
		}
	
		public void PrepareHierarchyPropertyConfigurations(MappingConfiguration<Hierarchy> configuration)
		{
			configuration.HasProperty(x => x.Id).IsIdentity(KeyGenerator.Autoinc).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Id").IsNotNullable().HasColumnType("int").HasPrecision(0).HasScale(0);
			configuration.HasProperty(x => x.FieldId).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("FieldId").IsNotNullable().HasColumnType("int").HasPrecision(0).HasScale(0);
			configuration.HasProperty(x => x.ParentId).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("ParentId").IsNotNullable().HasColumnType("int").HasPrecision(0).HasScale(0);
			configuration.HasProperty(x => x.Caption).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Caption").IsNullable().HasColumnType("varchar").HasLength(100);
			configuration.HasProperty(x => x.Cardinality).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Cardinality").IsNullable().HasColumnType("int").HasPrecision(0).HasScale(0);
			configuration.HasProperty(x => x.DefaultMember).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("DefaultMember").IsNullable().HasColumnType("varchar").HasLength(200);
		}
	
		public void PrepareHierarchyAssociationConfigurations(MappingConfiguration<Hierarchy> configuration)
		{
			configuration.HasAssociation(x => x.Field).WithOpposite(x => x.Hierarchies).ToColumn("FieldId").HasConstraint((x, y) =>  x.FieldId == y.Id ).IsRequired().WithDataAccessKind(DataAccessKind.ReadWrite);
			configuration.HasAssociation(x => x.ParentField).WithOpposite(x => x.ParentHierarchies).ToColumn("ParentId").HasConstraint((x, y) =>  x.ParentId == y.Id ).IsRequired().WithDataAccessKind(DataAccessKind.ReadWrite);
		}
		
		public MappingConfiguration<Field> GetFieldMappingConfiguration()
		{
			MappingConfiguration<Field> configuration = this.GetFieldClassConfiguration();
			this.PrepareFieldPropertyConfigurations(configuration);
			this.PrepareFieldAssociationConfigurations(configuration);

			return configuration;
		}

		public MappingConfiguration<Field> GetFieldClassConfiguration()
		{
			MappingConfiguration<Field> configuration = new MappingConfiguration<Field>();
			configuration.MapType(x => new { }).WithConcurencyControl(OptimisticConcurrencyControlStrategy.Changed).ToTable("Field");
	
			return configuration;
		}
	
		public void PrepareFieldPropertyConfigurations(MappingConfiguration<Field> configuration)
		{
			configuration.HasProperty(x => x.Id).IsIdentity(KeyGenerator.Autoinc).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Id").IsNotNullable().HasColumnType("int").HasPrecision(0).HasScale(0);
			configuration.HasProperty(x => x.MeasureGroupId).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("MeasureGroupId").IsNotNullable().HasColumnType("int").HasPrecision(0).HasScale(0);
			configuration.HasProperty(x => x.Name).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Name").IsNotNullable().HasColumnType("varchar").HasLength(100);
			configuration.HasProperty(x => x.Caption).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Caption").IsNullable().HasColumnType("varchar").HasLength(100);
			configuration.HasProperty(x => x.Type).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Type").IsNullable().HasColumnType("varchar").HasLength(50);
			configuration.HasProperty(x => x.IsMeasure).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("IsMeasure").IsNullable().HasColumnType("bit").HasPrecision(0).HasScale(0);
			configuration.HasProperty(x => x.IsCore).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("IsCore").IsNullable().HasColumnType("bit").HasPrecision(0).HasScale(0);
			configuration.HasProperty(x => x.Sort).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Sort").IsNullable().HasColumnType("smallint").HasPrecision(0).HasScale(0);
		}
	
		public void PrepareFieldAssociationConfigurations(MappingConfiguration<Field> configuration)
		{
			configuration.HasAssociation(x => x.MeasureGroup).WithOpposite(x => x.Fields).ToColumn("MeasureGroupId").HasConstraint((x, y) =>  x.MeasureGroupId == y.Id ).IsRequired().WithDataAccessKind(DataAccessKind.ReadWrite);
			configuration.HasAssociation(x => x.Hierarchies).WithOpposite(x => x.Field).ToColumn("FieldId").HasConstraint((y, x) =>  x.FieldId == y.Id ).WithDataAccessKind(DataAccessKind.ReadWrite);
			configuration.HasAssociation(x => x.ParentHierarchies).WithOpposite(x => x.ParentField).ToColumn("ParentId").HasConstraint((y, x) =>  x.ParentId == y.Id ).WithDataAccessKind(DataAccessKind.ReadWrite);
		}
		
		public MappingConfiguration<Cube> GetCubeMappingConfiguration()
		{
			MappingConfiguration<Cube> configuration = this.GetCubeClassConfiguration();
			this.PrepareCubePropertyConfigurations(configuration);
			this.PrepareCubeAssociationConfigurations(configuration);

			return configuration;
		}

		public MappingConfiguration<Cube> GetCubeClassConfiguration()
		{
			MappingConfiguration<Cube> configuration = new MappingConfiguration<Cube>();
			configuration.MapType(x => new { }).WithConcurencyControl(OptimisticConcurrencyControlStrategy.Changed).ToTable("Cube");
	
			return configuration;
		}
	
		public void PrepareCubePropertyConfigurations(MappingConfiguration<Cube> configuration)
		{
            // .HasFieldName("Id")
            configuration.HasProperty(x => x.Id).IsIdentity(KeyGenerator.Autoinc).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Id").IsNotNullable().HasColumnType("int").HasPrecision(0).HasScale(0);
			configuration.HasProperty(x => x.ConnectionId).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("ConnectionId").IsNotNullable().HasColumnType("int").HasPrecision(0).HasScale(0);
			configuration.HasProperty(x => x.Name).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Name").IsNullable().HasColumnType("varchar").HasLength(100);
			configuration.HasProperty(x => x.Caption).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Caption").IsNullable().HasColumnType("varchar").HasLength(100);
			configuration.HasProperty(x => x.Description).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Description").IsNullable().HasColumnType("varchar").HasLength(200);
			configuration.HasProperty(x => x.Active).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Active").IsNullable().HasColumnType("bit").HasPrecision(0).HasScale(0);
			configuration.HasProperty(x => x.LastUpdate).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("LastUpdate").IsNullable().HasColumnType("datetime2");
		}
	
		public void PrepareCubeAssociationConfigurations(MappingConfiguration<Cube> configuration)
		{
			configuration.HasAssociation(x => x.Connection).WithOpposite(x => x.Cubes).ToColumn("ConnectionId").HasConstraint((x, y) =>  x.ConnectionId == y.Id ).IsRequired().WithDataAccessKind(DataAccessKind.ReadWrite);
			configuration.HasAssociation(x => x.MeasureGroups).WithOpposite(x => x.Cube).ToColumn("CubeId").HasConstraint((y, x) =>  x.CubeId == y.Id ).WithDataAccessKind(DataAccessKind.ReadWrite);
		}
		
		public MappingConfiguration<Connection> GetConnectionMappingConfiguration()
		{
			MappingConfiguration<Connection> configuration = this.GetConnectionClassConfiguration();
			this.PrepareConnectionPropertyConfigurations(configuration);
			this.PrepareConnectionAssociationConfigurations(configuration);

			return configuration;
		}

		public MappingConfiguration<Connection> GetConnectionClassConfiguration()
		{
			MappingConfiguration<Connection> configuration = new MappingConfiguration<Connection>();
			//configuration.MapType(x => new { }).WithConcurencyControl(OptimisticConcurrencyControlStrategy.Changed).ToTable("Connection");
	
			return configuration;
		}
	
		public void PrepareConnectionPropertyConfigurations(MappingConfiguration<Connection> configuration)
		{
            configuration.HasProperty(x => x.Id).IsIdentity(KeyGenerator.Autoinc).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Id").IsNotNullable().HasColumnType("int").HasPrecision(0).HasScale(0);
			configuration.HasProperty(x => x.Name).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("Name").IsNullable().HasColumnType("varchar").HasLength(50);
			configuration.HasProperty(x => x.ConnectionString).WithDataAccessKind(DataAccessKind.ReadWrite).ToColumn("ConnectionString").IsNullable().HasColumnType("varchar").HasLength(500);
		}
	
		public void PrepareConnectionAssociationConfigurations(MappingConfiguration<Connection> configuration)
		{
            // .HasFieldName("Id")
            configuration.HasAssociation(x => x.Cubes).WithOpposite(x => x.Connection).ToColumn("ConnectionId").HasConstraint((y, x) =>  x.ConnectionId == y.Id ).WithDataAccessKind(DataAccessKind.ReadWrite);
		}
		
	}
}
