﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using Telerik.OpenAccess;
using Telerik.OpenAccess.FetchOptimization;

namespace Volap.Data.Repositories
{
    public interface IOpenAccessBaseRepository<TEntity, TContext> where TContext : OpenAccessContext, new()
    {
        IQueryable<TEntity> GetAll();

        PagingResponse<TEntity> GetAll(int skip, int take, string orderBy);

        TEntity GetBy(Expression<Func<TEntity, bool>> filter);

        TEntity AddNew(TEntity entity);

        TEntity Update(TEntity entity);

        void Delete(TEntity entity);

        // for paging
        IQueryable<TEntity> GetAllWithoutDetaching(); 
        IEnumerable<TEntity> DetachEntities(IQueryable<TEntity> entities);

    }

    public abstract partial class OpenAccessBaseRepository<TEntity, TContext> : IOpenAccessBaseRepository<TEntity, TContext>
        where TContext : OpenAccessContext, new()
    {
        protected TContext dataContext = new TContext();
        protected FetchStrategy fetchStrategy = new FetchStrategy();

        public virtual IQueryable<TEntity> GetAll()
        {
            List<TEntity> allEntities = dataContext.GetAll<TEntity>().Take(50).ToList();

            List<TEntity> detachedEntities = dataContext.CreateDetachedCopy(allEntities, fetchStrategy);
            //List<TEntity> detachedEntities = dataContext.CreateDetachedCopy<List<TEntity>>(allEntities, fetchStrategy);

            return detachedEntities.AsQueryable();
        }

        public virtual PagingResponse<TEntity> GetAll(int skip, int take, string orderBy)
        {
            IQueryable<TEntity> resultQuery = GetAllWithoutDetaching();
            int totalNumberOfRecords = resultQuery.Count();

            if (String.IsNullOrWhiteSpace(orderBy) == false)
            {
                try
                {
                    resultQuery = resultQuery.OrderBy(orderBy);
                }
                catch (ParseException ex) // when some of the order by parameters is invalid
                {
                    // return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, ex.Message);
                    // tpt - log error to file
                }
            }

            resultQuery = resultQuery.Skip(skip).Take(take);
            IEnumerable<TEntity> detachedResults = DetachEntities(resultQuery);
            return new PagingResponse<TEntity>(detachedResults, totalNumberOfRecords);
        }

        public virtual TEntity GetBy(Expression<Func<TEntity, bool>> filter)
        {
            if (filter == null)
                throw new ArgumentNullException("filter");

            TEntity entity = dataContext.GetAll<TEntity>().SingleOrDefault(filter);
            if (entity == null)
                return default(TEntity);

            TEntity detachedEntity = dataContext.CreateDetachedCopy(entity, fetchStrategy);

            return detachedEntity;
        }

        public virtual TEntity AddNew(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            TEntity attachedEntity = dataContext.AttachCopy(entity);
            dataContext.SaveChanges();
            TEntity detachedEntity = dataContext.CreateDetachedCopy(attachedEntity, fetchStrategy);

            return detachedEntity;
        }

        public virtual TEntity Update(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            TEntity attachedEntity = dataContext.AttachCopy(entity);
            dataContext.SaveChanges();
            TEntity detachedEntity = dataContext.CreateDetachedCopy(attachedEntity, fetchStrategy);

            return detachedEntity;
        }

        public virtual void Delete(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            TEntity attachedEntity = dataContext.AttachCopy(entity);
            dataContext.Delete(attachedEntity);
            dataContext.SaveChanges();
        }

        // paging
        public IQueryable<TEntity> GetAllWithoutDetaching()
        {
            IQueryable<TEntity> allEntities = dataContext.GetAll<TEntity>();
            return allEntities;
        }

        public IEnumerable<TEntity> DetachEntities(IQueryable<TEntity> entities)
        {
            IEnumerable<TEntity> detachedEntities = dataContext.CreateDetachedCopy<IEnumerable<TEntity>>(entities.ToList(), this.fetchStrategy);
            return detachedEntities;
        }
    }
}
