﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Volap.Data.Repositories
{
    //[JsonObject]
    [DataContract]
    public class PagingResponse<T> : IEnumerable<T>
    {
        private int? count;
        private IEnumerable<T> result;

        public PagingResponse(IEnumerable<T> result, int? count)
        {
            this.count = count;
            this.result = result;
        }

        [DataMember]
        public IEnumerable<T> Results
        {
            get { return result; }
        }

        [DataMember]
        public int? Count
        {
            get { return count; }
            set { this.count = value; }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return result.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return result.GetEnumerator();
        }
    }
}