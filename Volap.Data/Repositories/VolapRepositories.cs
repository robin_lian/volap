﻿using Volap.Data.Models;

namespace Volap.Data.Repositories
{
    public partial class ConnectionRepository : OpenAccessBaseRepository<Connection, FluentModel>
    {

    }

    public partial class CubeRepository : OpenAccessBaseRepository<Cube, FluentModel>
    {

    }

    public partial class MeasureGroupRepository : OpenAccessBaseRepository<MeasureGroup, FluentModel>
    {

    }


    public partial class MeasureRepository : OpenAccessBaseRepository<Measure, FluentModel>
    {
        
    }

    public partial class FieldRepository : OpenAccessBaseRepository<Field, FluentModel>
    {

    }

}
