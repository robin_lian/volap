namespace Volap.Data.Search
{
    public class Facet
    {

        public string Key { get; set; }

        public int Count { get; set; }

        public double Value { get; set; }
    }
}
