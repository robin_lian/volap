﻿using System;
using ServiceStack.Text;

namespace Volap.Data.Search
{
    public static class FacetExtensions
    {

        // get latest date from body in 1st hit 
        public static string GetLastDate(this JsonArrayObjects jsonArrayObjects, string dateColumn)
        {
            string date = null;

            foreach (var jsonObject in jsonArrayObjects)
            {
                foreach (var jsonObj in jsonObject.ArrayObjects("_source")) 
                    date = jsonObj.JsonTo<string>(dateColumn);
            }

            return date;
        }


        // bug es2.0 get latest date from _sort
        public static string GetLastDateOld(this JsonArrayObjects jsonArrayObjects, string type)
        {
            string date = null;

            foreach (var jsonObject in jsonArrayObjects)
            {
                date = jsonObject.JsonTo<string>("sort");
            }

            if (!string.IsNullOrEmpty(date))
                date = date.Replace("[ ", "").Replace(" ]", "");

            return date;
        }

    }
}
