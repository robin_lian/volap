using System.Text;

namespace Volap.Data.Search
{
    public class OlapContract
    {
        public string Index;

        public string MeasureGroup;

        
        public string Slicer1; // for slicer 1

        public string Slicer2;

        public string Slicer3;

        public string GroupBy; // for x dim

        public string Series; // for y dim

        public string Measure;

        public string MeasureType;

        public string Filter; // for measure filter in mdx

        public string Sort { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }

        public string[] CoreFields { get; set; }

        public string[] AllFields { get; set; }


        public OlapContract()
        {
            Page = 1;
            PageSize = 10;
        }

        /*private void InitType(string type)
        {
            switch (type)
            {
                case "incident"  :
                    AllFields = new[] {"assigned_to", "assignment_group", "description", "contact_type"};
                    CoreFields = new[] {"assignment_group", "description", "contact_type", "from_date"};
                    break;
                case "log":
                    AllFields = new[] { "assigned_to", "assignment_group", "description", "contact_type" };
                    CoreFields = new[] { "assignment_group", "description", "contact_type", "from_date" };
                    break;
            }
        }*/

        // url frenly eg "contact_type,assignment_group,description"
        public string GetCoreFields()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var field in CoreFields)
            {
                sb.Append(field).Append(",");
            }
            if (sb.ToString().EndsWith(","))
                sb.Remove(sb.Length - 1, 1);

            return sb.ToString(); 
        }
    }
}
