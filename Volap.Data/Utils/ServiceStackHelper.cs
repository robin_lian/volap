﻿using System;
using ServiceStack;

namespace Volap.Data.Utils
{
    public static class ServiceStackHelper
    {
        static ServiceStackHelper()
        {
            PclExport.Instance = new MyNet40PclExport();
            Licensing.RegisterLicense(string.Empty);
        }

        public static void Help()
        {

        }

        private class MyNet40PclExport : PclExport
        {
            public override LicenseKey VerifyLicenseKeyText(string licenseKeyText)
            {
                return new LicenseKey { Expiry = DateTime.MaxValue, Hash = string.Empty, Name = "Robin", Ref = "1", Type = LicenseType.Enterprise };
            }

            public override string ReadAllText(string filePath)
            {
                //base.ReadAllText(filePath);
                return "";
            }
        }
    }
}
