﻿//using Elasticsearch.Net.Connection.Thrift;
using System;
using System.Configuration;
using System.Diagnostics;
using Nest;

namespace Volap.Data
{
	public static class VolapConfig
	{
		public static readonly string DefaultIndex = "indexname" + "-" + Process.GetCurrentProcess().Id;

		public static Uri CreateBaseUri(int? port = null)
		{
            string host = ConfigurationManager.AppSettings["ElasticSearchUri"];
		    if (string.IsNullOrEmpty(host))
		        host = "localhost";
            
			//if (port == null && Process.GetProcessesByName("fiddler").Any()) host = "ipv4.fiddler";

			var uri = new UriBuilder("http", host, port.GetValueOrDefault(9200)).Uri;
			return uri;
		}

		public static ConnectionSettings Settings(int? port = null)
		{
            // todo ExposeRawResponse missing
            // DisableDirectStreaming -> for response.ResponseBodyInBytes
            return new ConnectionSettings(CreateBaseUri(port)).SetDefaultIndex(DefaultIndex).RequestTimeout(TimeSpan.FromMinutes(60)).
                PingTimeout(TimeSpan.FromMinutes(30)).PrettyJson().ThrowExceptions().DisableDirectStreaming();  // SetConnectTimeout .SetMaximumAsyncConnections(20)
        }

		public static readonly ElasticClient Client = new ElasticClient(Settings());
		//public static readonly ElasticClient ClientNoRawResponse = new ElasticClient(Settings().ExposeRawResponse(false));  no raw
		//public static readonly ElasticClient ClientThatThrows = new ElasticClient(Settings().ThrowOnElasticsearchServerExceptions());
		//public static readonly ElasticClient ThriftClient = new ElasticClient(Settings(9500), new ThriftConnection(Settings(9500)));

        
		public static string NewUniqueIndexName()
		{
			return DefaultIndex + "_" + Guid.NewGuid();
		}

	}
}