﻿using System.Configuration;
using Volap.Data.Access;

namespace Volap.Indexer
{
    class Program
    {
        static void Main(string[] args)
        {
            
            string method = ConfigurationManager.AppSettings["method"];
            switch (method)
            {
                case "Writer":
                    VolapWriter writer = new VolapWriter();
                    break;

                case "Reader":
                    VolapReader reader = new VolapReader();
                    break;

            }
        }
    }
}
