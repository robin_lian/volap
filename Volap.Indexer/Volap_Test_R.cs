﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using NLog;
using NUnit.Framework;
using Volap.Data.Access;
using Volap.Data.Search;

namespace Volap.Indexer
{

    [TestFixture]
    public class Volap_Test_R
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly Stopwatch _stopWatch = new Stopwatch();

        private OlapContract _olapContract;
        private SearchContract _searchContract;

        private IList<Facet> facets;
        private IList<Doc> docs;

        private OlapContract GetQuery(int measureGroupId)
        {
            _olapContract = new OlapContract();

            switch (measureGroupId)
            {
                case 1:
                    _olapContract.Index = "arsystem";
                    _olapContract.MeasureGroup = "incidents";
                    break;

            }

            return _olapContract;
        }

        private SearchContract GetSearch(int measureGroupId)
        {
            _searchContract = new SearchContract
            {
                DateField = "last_modified_date-date", // cos date dim
                FromDate = new DateTime(2013, 1, 1)
            };

            switch (measureGroupId)
            {
                case 1:
                    _searchContract.Index = "arsystem";
                    _searchContract.MeasureGroup = "incidents";
                    break;

            }

            return _searchContract;
        }     

        #region olap

        [Test]
        public void Test_Incidents_R_Count()
        {
            _olapContract = GetQuery(1);
            _olapContract.Slicer1 = "region:APAC";
            //olapDesc.Slicer2 = "department:Regional ICT,Project";
            _olapContract.GroupBy = "site";
            _olapContract.Measure = "incident_number";
            _olapContract.MeasureType = "count";
            //olapDesc.Filter = "totalreselapsebusinesstime>3600";

            var reader = new VolapReader(_olapContract);
            //facets = reader.GetFacets();


        }

        [Test]
        public void Test_Incidents_R_Filter1()
        {
            _olapContract = GetQuery(1);
            _olapContract.Slicer1 = "region:APAC";
            //olapDesc.Slicer2 = "department:Regional ICT,Project";
            _olapContract.GroupBy = "site";
            _olapContract.Measure = "incident_number";
            _olapContract.MeasureType = "count";
            _olapContract.Filter = "totalreselapsebusinesstime<=14400";

            var reader = new VolapReader(_olapContract);
            //facets = reader.GetFacets();


        }

        [Test]
        public void Test_Incidents_R_Filter2()
        {
            _olapContract = GetQuery(1);
            _olapContract.Slicer1 = "region:APAC";
            //olapDesc.Slicer2 = "department:Regional ICT,Project";
            _olapContract.GroupBy = "site";
            _olapContract.Measure = "totalreselapsebusinesstime";
            _olapContract.MeasureType = "sum";
            _olapContract.Filter = "totalreselapsebusinesstime<=14400";

            var reader = new VolapReader(_olapContract);
            //facets = reader.GetFacets();


        }


        [Test]
        public void Test_Incidents_R_Count_Dates()
        {
            _olapContract = GetQuery(1);
            _olapContract.Slicer1 = "department:Regional ICT,Project";
            //olapDesc.Slicer2 = "status:5";
            _olapContract.GroupBy = "last_modified_date:week";
            _olapContract.Measure = "totalreselapsebusinesstime";
            _olapContract.MeasureType = "sum";
            _olapContract.Filter = "totalreselapsebusinesstime>3600";


        }

        #endregion




        #region search

        [Test]
        public void Test_Incidents_R_Search_All1()
        {
            _searchContract = GetSearch(1);
            _searchContract.SetQuery("APAC");

            var search = new SearchInvoker(_searchContract);
            docs = search.GetDocs();

        }


        [Test]
        public void Test_Incidents_R_Search_Facets1()
        {
            _searchContract = GetSearch(1);
            _searchContract.SetQuery("Region:\"APAC\"");

            var search = new SearchInvoker(_searchContract);
            docs = search.GetDocs();

        }

        [Test]
        public void Test_Incidents_R_Search_Facets2()
        {
            _searchContract = GetSearch(1);
            _searchContract.ToDate = new DateTime(2013, 8, 8);
            _searchContract.SetQuery("Assigned_Group:\"Service Desk\"");
            //_searchContract.GetQueryFacets().Add("Assigned_Group:\"Service Desk\"");

            var search = new SearchInvoker(_searchContract);
            docs = search.GetDocs();

        }


        #endregion
    }
}
