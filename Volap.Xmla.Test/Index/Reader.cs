﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using Elasticsearch.Net;
using Nest;
using NLog;
using ServiceStack.Text;
using Volap.Data.Utils;
using Volap.Xmla.Test.Model;

namespace Volap.Xmla.Test.Index
{
    public class Reader
    {
        private ElasticClient client;
        private const int DocCount = 20;
        private const int FacetCount = 10;
        //private ConnectionSettings csettings;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private Stopwatch stopWatch;


        public Reader()
        {
            ServiceStackHelper.Help();
            stopWatch = new Stopwatch();
            var settings = new ConnectionSettings(new Uri(ConfigurationManager.AppSettings["server"])); //.SetDefaultIndex("incidents");
            client = new ElasticClient(settings);

            RunOlap();

            RunSearch();
        }

        private void RunOlap()
        {
            const int count = 200;

            // Search exists
            /*var searchDesc = new SearchDescriptor<object>();
            searchDesc.Index("winsecurity").Size(3).Query(q => q.Term("DATA", "information1"));
            ElasticsearchResponse<string> response1 = client.Raw.Search<string>(client.Serializer.Serialize(searchDesc));

            if (response1.Success && response1.Response != null)
            {
                var hresults = JsonObject.Parse(response1.Response);
                int hits = Convert.ToInt32(hresults.Object("hits").Get<string>("total"));
            }

            ElasticsearchResponse<DynamicDictionary> response2 = client.Raw.SearchExists("winapplication", "winapplication", "DATA:information");*/



            stopWatch.Start();
            _logger.Info("Running Faceted Search : Business_Resolve_Time by Assignment_Group @ " + DateTime.Now);

            // list facets for all
            var result = client.Search<Incident>(s => s.Index("incidents").Query(q => q.MatchAll()).
                            FacetTerm("assignment_group", f => f.OnField("assignment_group").AllTerms()));   // get terms with 0 count too
            // .OnFields(new string[] { "assignment_group", "assigned_to" }).Size(100);

            Facet value = null;
            result.Facets.TryGetValue("assignment_group", out value);

            var termFacet = (TermFacet)value;
            Console.WriteLine("Members for assignment_group");
            if (termFacet != null)
                foreach (TermItem item in termFacet.Items)
                {
                    Console.WriteLine("term {0}", item.Term);  // TotalCount
                }

            // count only, or for text
            /*var result = client.Search<Incident>(s => s.Index("incidents").Query(q => q.Filtered(fq =>
                        fq.Filter(ff => ff.Term(f => f.Contact_Type, "phone")))).  // filter
                            FacetTerm(ft => ft.OnField(f => f.Location).Size(facetCount))
                         );  // x
            result.Facets.TryGetValue("location", out value);

            var termFacet = (TermFacet)value;
            if (termFacet != null)
                foreach (TermItem item in termFacet.Items)
                {
                    Console.WriteLine("term {0} : {1}", item.Term, item.Count);
                }
            stopWatch.Stop();
            _logger.Info(string.Format("Time taken {0} to search {1} results", stopWatch.Elapsed.TotalSeconds, count));
            stopWatch.Restart();*/

            // other measures
            result = client.Search<Incident>(s => s.Index("incidents").Query(q => q.Filtered(fq =>
                        fq.Filter(ff => ff.Term(f => f.Contact_Type, "phone")))).  // filter b4 facet 
                        FacetTermsStats(ts => ts.KeyField(f => f.Assignment_Group).ValueField(f => f.Business_Resolve_Time).Size(FacetCount))
                     );  // x-y    Order

            value = null;
            result.Facets.TryGetValue("assignment_group", out value);

            var termStatsFacet = (TermStatsFacet)value;
            if (termStatsFacet != null)
                foreach (TermStats item in termStatsFacet.Items) 
                {
                    Console.WriteLine("term {0} : {1}", item.Term, item.Mean);  // TotalCount
                }

            stopWatch.Stop();
            _logger.Info(string.Format("Time taken {0} to get FacetTermsStats", stopWatch.Elapsed.TotalSeconds));
            stopWatch.Restart();

            result = client.Search<Incident>(s => s.Index("incidents").  // FacetFilter  
                        FacetTermsStats(ts => ts.KeyField(f => f.Assignment_Group).ValueField(f => f.Business_Resolve_Time).Size(FacetCount).
                        FacetFilter(ff => ff.Term(f => f.Contact_Type, "phone")))
                     );  // x-y    Order

            value = null;
            result.Facets.TryGetValue("assignment_group", out value);

            termStatsFacet = (TermStatsFacet)value;
            if (termStatsFacet != null)
                foreach (TermStats item in termStatsFacet.Items)
                {
                    Console.WriteLine("term {0} : {1}", item.Term, item.Mean);  // TotalCount
                }

            stopWatch.Stop();
            _logger.Info(string.Format("Time taken {0} to get FacetTermsStats via FacetFilter", stopWatch.Elapsed.TotalSeconds));
            stopWatch.Restart();


            // Last N Time periods, use past N to 1 completed time indices
            // loop around X, get value for each X,   or put in facet_filter

            result = client.Search<Incident>(s => s.Index("incidents").Filter(f => new FilterDescriptor<Incident>().
                Bool(b => b.Must(m => m.Term(i => i.Contact_Type, "phone"), m => m.Term(i => i.Assignment_Group, " Desktop Support")))));

            int row = 1;
            double measure = 0;
            foreach (Incident inc in result.Documents)
            {
                measure += inc.Business_Resolve_Time;
                row++;
            }
            _logger.Info("Average Business Resolve Time " + measure / row);

            stopWatch.Stop();
            _logger.Info(string.Format("Time taken {0} to get measures", stopWatch.Elapsed.TotalSeconds));


            /*result = client.Search<Incident>(s => s.Index("incidents").Query(q => q.Filtered(fq =>
                        fq.Filter(ff => ff.Term(f => f.Contact_Type, "phone")))).  // filter
                            FacetDateHistogram(h => h.OnField(f => f.From_Date).Interval(DateInterval.Hour).TimeZone("11"))
                         );
            value = null;
            result.Facets.TryGetValue("location", out value);

            var dateHistFacet = (DateHistogramFacet)value;
            if (dateHistFacet != null)
                foreach (DateEntry item in dateHistFacet.Items)
                {
                    Console.WriteLine("term {0} : {1}", item.Count, item.Count);
                }

            stopWatch.Stop();
            _logger.Info(string.Format("Time taken {0} to search {1} results", stopWatch.Elapsed.TotalSeconds, count));
            stopWatch.Restart();*/

            _logger.Info("End Olap Search");
        }

        // text search with date filter
        private void RunSearch()
        {
            _logger.Info("Running Text Search : @ " + DateTime.Now);
            stopWatch.Restart();

            string index = "incidents";
            string type = "incident";
            
            var sqd = new SearchQueryDescriptor("incident")
            {
                FromDate = new DateTime(2011, 10, 23),
                //ToDate = new DateTime(2011, 10, 22),
            };

            sqd.SetQuery("phone desk assignment_group:\"Desk\"");

            //var searchDesc = new SearchDescriptor<Incident>().Query(q => q.Filtered(qf => 
            //    qf.Query(fq => fq.QueryString(qs => qs.Query(sqd.QueryString)))));

            /* use MultiMatch with diff field arrays, use _all?
                .Query(q => q.MultiMatch(mm => mm.OnFields(sqd.AllFields).QueryString("phone")) &&
                q.MultiMatch(mm => mm.OnFields(new[] { "assignment_group" }).QueryString(" Service Desk"))); 
             */
            ElasticsearchResponse<string> response = null;  
            List<Hit> hits = null; // to store in cache  DynamicDictionary
            List<Doc> docs = null;

            // tpt add more search objects
            switch (type)
            {
                case "incident":
                    response = GetSearchResponse(sqd);
                    break;
            }

            if (response.Success && response.Response != null)
            {
                // get docids from hits json
                var hresults = JsonObject.Parse(response.Response);

                hits = hresults.Object("hits").ArrayObjects("hits").ConvertAll(x => new Hit
                {
                    Id = x.JsonTo<int>("_id"),
                    //Score = x.JsonTo<float>("_score")
                });

                // get multi-get docs using only core fields
                ElasticsearchResponse<string> docResponse = client.Raw.Mget<string>(index, type, new { ids = hits.Select(h => h.Id).ToArray() },
                                                                q => q.Fields(sqd.GetCoreFields()) );

                if (docResponse.Success && docResponse.Response != null)
                {
                    var hitsArr = JsonObject.Parse(docResponse.Response);

                    docs = hitsArr.ArrayObjects("docs").ConvertAll(x => new Doc
                           {
                               Id = x.JsonTo<int>("_id"),
                               Fields = x.Get("fields")
                           });
                }
            }

            if (docs != null && docs.Any())
            {
                foreach (Doc doc in docs)
                {
                    string fields = doc.Fields;
                    fields = fields.Substring(1, fields.Length - 2); // remove { }
                    string[] fArr = fields.Split(',');
                    foreach (var f in fArr)
                    {
                        
                    }
                }
            }

            _logger.Info(string.Format("Time taken {0} for text search with date range", stopWatch.Elapsed.TotalSeconds));
            stopWatch.Restart();


            // top values | top limit=20 assignment_group -> display of chart count on top N facet terms
            var result = client.Search<Incident>(s => s.Index("incidents").Query(q => q.Filtered(fq =>
                         fq.Filter(ff => ff.Term(f => f.Contact_Type, "phone")))).
                         FacetTermsStats(ts => ts.KeyField(f => f.Location).ValueScript("1").Size(FacetCount)));

            Facet value;
            result.Facets.TryGetValue("location", out value);

            var termStatsFacet = (TermStatsFacet)value;
            if (termStatsFacet != null)
                foreach (TermStats item in termStatsFacet.Items)
                {
                    Console.WriteLine("term {0} : {1}", item.Term, item.Count);  // TotalCount
                }

            _logger.Info("End Text Search");
        }

        // get connection from incident search
        private ElasticsearchResponse<string> GetSearchResponse(SearchQueryDescriptor sqd)
        {
            var searchDesc = new SearchDescriptor<Incident>();
            searchDesc.Query(q => q.Bool(mq => mq.Should(GenerateQueries(sqd).ToArray())));

            if (sqd.HasQueryDates()) SetQueryDates(sqd, searchDesc);

            // sort action - column and sort type from grid
            searchDesc.Sort(s => s.OnField("assigned_to").Ascending());

            // paging
            searchDesc.Take(DocCount);

            return client.Raw.Search<string>(client.Serializer.Serialize(searchDesc));
        }

        private IEnumerable<Func<QueryDescriptor<Incident>, QueryContainer>> GenerateQueries(SearchQueryDescriptor sqd)
        {
            Func<QueryDescriptor<Incident>, QueryContainer> a = null;

            // match :
            if (!string.IsNullOrWhiteSpace(sqd.QueryString))
            {
                a = (q) => q.Filtered(f => f.Query(fq => fq.QueryString(qs => qs.Query(sqd.QueryString))));
                yield return a;
            }
            /*if (sqd.HasQueryTerms())
            {
                a = (q) => q.MultiMatch(mm => mm.OnFields(new[] { "contact_type" }).QueryString("phone"));
                yield return a;
            }*/

            // match all fields
            foreach (var query in sqd.GetQueryAll())
            {
                if (!string.IsNullOrWhiteSpace(query))
                {
                    a = (q) => q.MultiMatch(mm => mm.OnFields(sqd.AllFields).Query(query));
                    yield return a;
                }
            }
        }

        private void SetQueryDates(SearchQueryDescriptor sqd, SearchDescriptor<Incident> sd)
        {
            if (sqd.FromDate == null)
                return;

            if (sqd.ToDate == null)
                sd.Filter(ff => ff.Range(r => r.OnField("from_date").GreaterOrEquals(sqd.FromDate)));

            else
                sd.Filter(ff => ff.Range(r => r.OnField("from_date").GreaterOrEquals(sqd.FromDate)) &&
                          ff.Range(r => r.OnField("from_date").LowerOrEquals(sqd.ToDate)));
        }

        private void SetFilterFacets(SearchQueryDescriptor sqd, SearchDescriptor<Incident> sd)
        {
            sd.FacetFilter("location", ff => ff.Term(f => f.Location, "true"));
        }

        // CreatePropSelector<Incident>("location")
        Expression<Func<T, object>> CreatePropSelector<T>(string propertyName)
        {
            var parameter = Expression.Parameter(typeof(T));
            var body = Expression.Convert(Expression.PropertyOrField(parameter, propertyName), typeof(object));
            return Expression.Lambda<Func<T, object>>(body, parameter);
        }

    }
}
