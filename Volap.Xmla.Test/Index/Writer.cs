﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using Elasticsearch.Net;
using Nest;
using NLog;
using Telerik.OpenAccess;
using Volap.Data;
using Volap.Xmla.Test.Model;

namespace Volap.Xmla.Test.Index
{
    public class Writer
    {

        OpenAccessContext volapContext;

        private ElasticClient client;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private Stopwatch stopWatch;
        private int rowCount;

        public Writer()
        {
            InitVolap();

            //WriteLog();
        }

        void InitVolap()
        {
            volapContext = new VolapModel();
            stopWatch = new Stopwatch();

            var setting = new ConnectionSettings(new Uri(ConfigurationManager.AppSettings["server"]));
            client = new ElasticClient(setting);

            const string sqlQuery = @"Select * from Cube where Active=1";
            IList<Cube> cubes = volapContext.ExecuteQuery<Cube>(sqlQuery);
            Dictionary<int, DateTime> cubeDict = new Dictionary<int, DateTime>();

            foreach (Cube cube in cubes)
            {
                Console.WriteLine("CubeId: {0}\n", cube.Id);
                if (cube.MinDate != null)
                {
                    DateTime time = cube.LastUpdate ?? (DateTime)cube.MinDate;
                    cubeDict.Add(cube.Id, time);
                }
            }

            foreach (int id in cubeDict.Keys)
            {
                DateTime date;
                if (cubeDict.TryGetValue(id, out date))
                    WriteIndex(id, date);
            }

            // tpt add scheduler and optimize at end (daily, weekly, monthly)
            string index = "incidents";

            var r = client.Optimize(o => o.Index(index));
            if (r.ConnectionStatus.Success)
                _logger.Info("index {0} optimized", index);

            //ElasticsearchResponse<string> response = client.Raw.IndicesOptimize<string>(index, o => o.MaxNumSegments(1));
            //if (response.Success)

            Console.WriteLine();
        }

        private void WriteLog()
        {
            throw new NotImplementedException();
        }

        private void WriteIndex(int cubeId, DateTime date)
        {
            string sqlQuery = @"Select * from MeasureGroup where CubeId=" + cubeId;
            var measureGroups = volapContext.ExecuteQuery<MeasureGroup>(sqlQuery);

            foreach (MeasureGroup measureGroup in measureGroups)
            {
                string dateCol = measureGroup.DateColumn;
                string name = measureGroup.Name;

                // if not exist index, create new
                var r = client.IndexExists(f => f.Index(measureGroup.Name.ToLower()));
                if (r.Exists) 
                {
                    if (measureGroup.Name.IndexOf("incid", StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        client.CreateIndex(name.ToLower(), c => c
                            .NumberOfShards(1).NumberOfReplicas(0)
                            .Settings(s => s.Add("merge.policy.merge_factor", "10").
                                             Add("search.slowlog.threshold.fetch.warn", "1s").
                                             Add("index.auto_expand_replicas", "false").
                                             Add("index.refresh_interval", "30s").
                                             Add("index.store.type", "mmapfs")
                                     )
                            .AddMapping<Incident>(m => m.MapFromAttributes().
                                AllField(a => a.Enabled(false).SearchAnalyzer("standard").IndexAnalyzer("standard")).   //whitespace  
                                NumericDetection().DateDetection().
                                SourceField(s => s.Enabled(false).Compress(false)))
                        );
                    }
                }

                using (IDbConnection oaConnection = volapContext.Connection)
                {
                    string SqlQueryString = measureGroup.NamedQuery + " where " + dateCol + " > @date";

                    using (IDbCommand oaCommand = oaConnection.CreateCommand())
                    {
                        IDbDataParameter dataParameter = oaCommand.CreateParameter();
                        dataParameter.ParameterName = "@date";
                        dataParameter.DbType = DbType.DateTime;
                        dataParameter.Value = date;

                        oaCommand.CommandText = SqlQueryString;
                        oaCommand.Parameters.Add(dataParameter);

                        stopWatch.Restart();
                        _logger.Info("Writing Index {0} at {1} ", name.ToLower(), DateTime.Now);

                        client.IndexMany(WriteValues(name.ToLower(), oaCommand), name.ToLower(), name.Substring(0, name.Length - 1).ToLower());

                        stopWatch.Stop();
                        _logger.Info(string.Format("Time taken {0} to write {1} rows", stopWatch.Elapsed.TotalSeconds, rowCount));
                    }
                }
            }
        }

        private IEnumerable<Incident> WriteValues(string measureGroupName, IDbCommand oaCommand)
        {
            var rows = new List<Incident>(); // dynamic
            rowCount = 1;

            using (IDataReader reader = oaCommand.ExecuteReader())
            {
                while (reader.Read())
                {
                    dynamic row = null;
                    if (measureGroupName.IndexOf("incid", StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        row = new Incident
                        {
                            Id = rowCount,
                            TransactionId = reader.IsDBNull(0) ? "" : reader.GetString(0),
                            TransactionTypeId = reader.IsDBNull(1) ? 0 : reader.GetInt32(1),
                            From_Date = reader.IsDBNull(2) ? DateTime.UtcNow : reader.GetDateTime(2),
                            Assigned_To = reader.IsDBNull(3) ? "" : reader.GetString(3),
                            Assignment_Group = reader.IsDBNull(4) ? "" : reader.GetString(4),
                            Business_Resolve_Time = reader.IsDBNull(5) ? 0 : reader.GetDouble(5),
                            Contact_Type = reader.IsDBNull(6) ? "" : reader.GetString(6),
                            Description = reader.IsDBNull(7) ? "" : reader.GetString(7),
                            Impact = reader.IsDBNull(8) ? 0 : TryToParse(reader.GetString(8)),
                            Incident_State = reader.IsDBNull(9) ? 0 : TryToParse(reader.GetString(9)),
                            Location = reader.IsDBNull(10) ? "" : reader.GetString(10),
                            Number = reader.IsDBNull(11) ? "" : reader.GetString(11),
                            Source = reader.IsDBNull(12) ? "" : reader.GetString(12),
                            Urgency = reader.IsDBNull(13) ? 0 : TryToParse(reader.GetString(13)),
                            Active = !reader.IsDBNull(14) && bool.Parse(reader.GetString(14))
                        };
                    }
                    rowCount++;
                    if(row!=null)
                        rows.Add(row);
                }
            }
            return rows;
        }

        private int TryToParse(string value)
        {
            int number = 0;
            bool result = Int32.TryParse(value, out number);
            if (result)
            {
                //Console.WriteLine("Converted '{0}' to {1}.", value, number);
            }
            else
            {
                if (value == null) value = "";
                Console.WriteLine("Attempted conversion of '{0}' failed.", value);
            }
            return number;
        }

    }
}
