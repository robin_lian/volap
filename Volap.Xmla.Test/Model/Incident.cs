﻿using System;
using Nest;

namespace Volap.Xmla.Test.Model
{
    [ElasticType(Name = "incident")]
        /*, DateDetection = true, NumericDetection = true,
        DynamicDateFormats = new[] { "sqlDate", "yyyy-MM-dd HH:mm:ss.SSS" } // ||yyyy/MM/dd Z*/
   

    public class Incident
    {

        //[ElasticProperty(Index = FieldIndexOption.not_analyzed, Store = false)]
        public int Id { get; set; }

        [ElasticProperty(Index = FieldIndexOption.NotAnalyzed, Store = true)]  //  Store = true only for measures
        public string TransactionId { get; set; }

        [ElasticProperty(NumericType = NumberType.Integer)]
        public int TransactionTypeId { get; set; }

        [ElasticProperty(Name = "from_date", DateFormat = "sqlDate", Store = true)]
        public DateTime From_Date { get; set; }

        [ElasticProperty(Index = FieldIndexOption.NotAnalyzed, Name = "assigned_to", Store = true)]
        public string Assigned_To { get; set; }

        [ElasticProperty(Index = FieldIndexOption.NotAnalyzed, Name = "assignment_group", Store = true)]
        public string Assignment_Group { get; set; }

        [ElasticProperty(NumericType = NumberType.Double, Name = "business_resolve_time", Store = true)]
        public double Business_Resolve_Time { get; set; }

        [ElasticProperty(Index = FieldIndexOption.NotAnalyzed, Name = "contact_type", Store = true)]
        public string Contact_Type { get; set; }

        [ElasticProperty(Index = FieldIndexOption.Analyzed, Store = true)]
        public string Description { get; set; }

        [ElasticProperty(NumericType = NumberType.Integer, Store = true)]
        public int Impact { get; set; }

        [ElasticProperty(NumericType = NumberType.Integer, Name = "incident_state")]
        public int Incident_State { get; set; }

        [ElasticProperty(Index = FieldIndexOption.NotAnalyzed, Store = true)]
        public string Location { get; set; }

        [ElasticProperty(Index = FieldIndexOption.NotAnalyzed, Store = true)]
        public string Number { get; set; }

        [ElasticProperty(Index = FieldIndexOption.NotAnalyzed, Store = true)]
        public string Source { get; set; }

        [ElasticProperty(NumericType = NumberType.Integer, Store = true)]
        public int Urgency { get; set; }

        [ElasticProperty(Type = FieldType.Boolean)]
        public bool Active { get; set; }

    }
}