﻿
namespace Volap.Xmla.Test.Model
{
    using System;
    using Nest;

    namespace Volap.Xmla.Test.Model
    {
        [ElasticType(Name = "log")]

        public class Log
        {

            public int Id { get; set; }

            [ElasticProperty(Index = FieldIndexOption.NotAnalyzed, Store = true)]  //  Store = true only for measures
            public string Source { get; set; }

            [ElasticProperty(Index = FieldIndexOption.NotAnalyzed, Name = "source_host", Store = true)] 
            public string Source_Host { get; set; }

            [ElasticProperty(Index = FieldIndexOption.NotAnalyzed, Name = "source_path", Store = true)] 
            public string Source_Path { get; set; }

            [ElasticProperty(DateFormat = "sqlDate", Store = true)]
            public DateTime TimeStamp { get; set; }

            [ElasticProperty(Index = FieldIndexOption.NotAnalyzed, Store = true)]
            public string Type { get; set; }

            [ElasticProperty(Index = FieldIndexOption.Analyzed, Store = true)]
            public string Message { get; set; }

        }
    }
}
