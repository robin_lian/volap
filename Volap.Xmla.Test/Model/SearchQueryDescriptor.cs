﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Volap.Xmla.Test.Model
{
    public class SearchQueryDescriptor
    {

        public string QueryString;
        public string Sort { get; set; }

        //selected facets
        public Dictionary<string, List<string>> Facets { get; set; }
        public int Page { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public int PageSize { get; set; }


        private List<string> queryAll = new List<string>();

        //private bool useFacetting = false;

        private bool hasQueryAll = false;

        private string[] CoreFields { get; set; }

        public string[] AllFields { get; set; }


        public SearchQueryDescriptor(string type)
        {
            InitType(type);

            Facets = new Dictionary<string, List<string>>();
            Page = 1;
            PageSize = 10;
        }

        private void InitType(string type)
        {
            switch (type)
            {
                case "incident"  :
                    AllFields = new[] {"assigned_to", "assignment_group", "description", "contact_type"};
                    CoreFields = new[] {"assignment_group", "description", "contact_type", "from_date"};
                    break;
                case "log":
                    AllFields = new[] { "assigned_to", "assignment_group", "description", "contact_type" };
                    CoreFields = new[] { "assignment_group", "description", "contact_type", "from_date" };
                    break;
            }
        }

        public bool HasQueryAll()
        {
            return hasQueryAll;
        }

        public IList<string> GetQueryAll()
        {
            return queryAll;
        }

        public bool HasQueryDates()
        {
            return FromDate != null || ToDate != null;
        }

        // http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html
        public void SetQuery(string query)
        {
            if (!String.IsNullOrEmpty(query))
            {
                StringBuilder sb = new StringBuilder();
                //query = query.ToLower();
                string[] searchArr = query.Split(' ');
                bool end = true; // detect end of “

                foreach (var searchAr in searchArr)
                {
                    if (searchAr.Contains(":"))
                    {
                        if (end)
                        {
                            // Events in this field   field:* => _exists_:field
                            if (searchAr.EndsWith("*"))
                                sb.Append(" _exists_:").Append(searchAr.Split(':')[0]);
                            else
                            {
                                sb.Append(" ").Append(searchAr);

                                if (searchAr.Contains("\"")) // " straight after :
                                    end = false;
                                else
                                    sb.Append(" "); // single f:term
                            }
                        }
                    }
                    else if (searchAr.Contains("|"))
                    {


                    }
                    else
                    {
                        if (searchAr.Length >= 1)
                        {
                            if (end)
                            {
                                hasQueryAll = true;
                                queryAll.Add(searchAr);

                                /*foreach (string d in AllFields)
                                {
                                    sb.Append(d).Append(":").Append(searchAr).Append("* OR ");
                                }
                                if (sb.ToString().EndsWith(" OR "))
                                    sb.Remove(sb.Length - 4, 4);
                                sb.Append(" ");*/
                            }
                            else
                            {
                                if (searchAr.Contains("\"")) // end closing "
                                {
                                    sb.Append(" ").Append(searchAr).Append(" ");
                                    end = true;
                                }
                                else // continue add term
                                    sb.Append(" ").Append(searchAr);
                            }
                        }
                    }
                }
                QueryString = sb.ToString();
            }
        }

        // url frenly eg "contact_type,assignment_group,description"
        public string[] GetCoreFields()
        {
            /*foreach (var field in CoreFields)
            {
                sb.Append(field).Append(",");
            }
            if (sb.ToString().EndsWith(","))
                sb.Remove(sb.Length - 1, 1);

            return sb.ToString(); */
            return CoreFields;
        }
    }
}
