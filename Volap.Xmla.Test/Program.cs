﻿using System.Configuration;
using Volap.Xmla.Test.Index;

namespace Volap.Xmla.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            string method = ConfigurationManager.AppSettings["method"];
            switch (method)
            {
                case "Writer":
                    new Writer();
                    break;

                case "Reader":
                    new Reader();
                    break;

                /*case "DundasXmlReader":
                    new DundasXmlReader(ConfigurationManager.AppSettings["inputPath"]);
                    break;*/

            }
        }
    }
}
