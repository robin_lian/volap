﻿
CREATE TABLE [dbo].[Connection](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[ConnectionString] [varchar](500) NULL
 CONSTRAINT [PK_Connection] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[Cube](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ConnectionId] [int] NOT NULL,
	[Name] [varchar](100) NULL,
	[Caption] [varchar](100) NULL,
	[Description] [varchar](200) NULL,
	[Active] [bit] NULL,
	[MinDate] [datetime2] NULL,
	[LastUpdate] [datetime2] NULL
	
 CONSTRAINT [PK_Cube] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Cube]  WITH CHECK ADD  CONSTRAINT [FK_Cube_Connection] FOREIGN KEY([ConnectionId])
REFERENCES [dbo].[Connection] ([Id])
GO

ALTER TABLE [dbo].[Cube] CHECK CONSTRAINT [FK_Cube_Connection]
GO


CREATE TABLE [dbo].[MeasureGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CubeId] [int] NOT NULL,
	[Name] [varchar](100) NULL,
	[NamedQuery] [varchar](1000) NULL,
	[DateColumn] [varchar](50) NULL
	
 CONSTRAINT [PK_MeasureGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MeasureGroup]  WITH CHECK ADD  CONSTRAINT [FK_MeasureGroup_Cube] FOREIGN KEY([CubeId])
REFERENCES [dbo].[Cube] ([Id])
GO

ALTER TABLE [dbo].[MeasureGroup] CHECK CONSTRAINT [FK_MeasureGroup_Cube]
GO


CREATE TABLE [dbo].[Measure](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MeasureGroupId] [int] NOT NULL,
	[Name] [varchar](100) NULL,
	[Caption] [varchar](100) NULL,
	[UniqueName] [varchar](100) NULL,
	[Aggregator] [smallint] NULL,
	[Visible] [bit] NULL,
	[DisplayFolder] [varchar](100) NULL,
	[Format] [varchar](100) NULL
	
 CONSTRAINT [PK_Measure] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Measure]  WITH CHECK ADD  CONSTRAINT [FK_Measure_MeasureGroup] FOREIGN KEY([MeasureGroupId])
REFERENCES [dbo].[MeasureGroup] ([Id])
GO

ALTER TABLE [dbo].[Measure] CHECK CONSTRAINT [FK_Measure_MeasureGroup]
GO



CREATE TABLE [dbo].[Dimension](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[Caption] [varchar](100) NULL,
	[Description] [varchar](100) NULL,
	[UniqueName] [varchar](100) NULL,
	[Core] [bit] NULL,

 CONSTRAINT [PK_Dimension] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[MeasureGroup_Dimension_Mapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MeasureGroupId] [int] NOT NULL,
	[DimensionId] [int] NOT NULL,
	[DisplayOrder] [int] NOT NULL
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MeasureGroup_Dimension_Mapping]  WITH CHECK ADD CONSTRAINT [MeasureGroupDim_MeasureGroup] FOREIGN KEY([MeasureGroupId])
REFERENCES [dbo].[MeasureGroup] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[MeasureGroup_Dimension_Mapping] CHECK CONSTRAINT [MeasureGroupDim_MeasureGroup]
GO

ALTER TABLE [dbo].[MeasureGroup_Dimension_Mapping] WITH CHECK ADD CONSTRAINT [MeasureGroupDim_Dim] FOREIGN KEY([DimensionId])
REFERENCES [dbo].[Dimension] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[MeasureGroup_Dimension_Mapping] CHECK CONSTRAINT [MeasureGroupDim_Dim]
GO


CREATE TABLE [dbo].[Hierarchy](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DimensionId] [int] NOT NULL,
	[Name] [varchar](100) NULL,
	[Caption] [varchar](100) NULL,
	[Description] [varchar](100) NULL,
	[UniqueName] [varchar](100) NULL,
	--[GroupName] [varchar](100) NULL,
	[Cardinality] [int] NULL,
	[DefaultMember] [varchar](200) NULL
	--[Visible] [bit] NULL
	
 CONSTRAINT [PK_Hierarchy] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Hierarchy] WITH CHECK ADD CONSTRAINT [Hierarchy_Dimension] FOREIGN KEY([DimensionId])
REFERENCES [dbo].[Dimension] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Hierarchy] CHECK CONSTRAINT [Hierarchy_Dimension]
GO


INSERT [dbo].[Connection] ([Name], [ConnectionString]) VALUES (N'Emite', N'Data Source=.;Initial Catalog=EmiteDemo;Integrated Security=False;User ID=yimai;Password=Yimai88;;MultipleActiveResultSets=True')
GO
INSERT [dbo].[Connection] ([Name], [ConnectionString]) VALUES (N'Log', N'Data Source=.;Initial Catalog=IndexSystem;Integrated Security=False;User ID=yimai;Password=Yimai88;;MultipleActiveResultSets=True')
GO

INSERT INTO Cube([ConnectionId],[Name],[Caption],[Description],[Active]) VALUES (1, 'Emite', 'Emite', 'Emite Cube', 1);
GO
INSERT INTO Cube([ConnectionId],[Name],[Caption],[Description],[Active]) VALUES (1, 'TransactionV2', 'TransactionV2', 'TransactionV2 Cube', 1);
GO


INSERT INTO MeasureGroup([CubeId],[Name],[NamedQuery],[DateColumn]) VALUES (1, 'Metric Values', 'select metricid, dateadd(hour,datediff(hour,0,Date),0) as date,cast(avg(value) as float) as value from metricvalues mv group by metricid, dateadd(hour,datediff(hour,0,Date),0)', 'date');
GO
INSERT INTO MeasureGroup([CubeId],[Name],[NamedQuery],[DateColumn]) VALUES (2, 'Incidents', 'select * from Transactionview', 'FromDate');
GO